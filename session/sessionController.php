<?php 

class SessionController 
{
	public function start () {
		session_start();
	}

	public function destroy () {
		$this->start();
		session_destroy();
	}

	public function user () {
		$this->start();

		if (!isset($_SESSION['user'])) {
			return false;
		}

		return $_SESSION['user'];
	}
}

?>