<?php require('../session/sessionController.php');   
$session = new sessionController(); 
$userInfo = $session->user();
$has_user = empty($userInfo) ? false : true;
?>
	<header>
		<nav class="navbar navbar-expand-lg navbar-light py-4">
			<div class="container">
				<h1>
					<a class="navbar-brand" href="home.php">
						<i class="fas fa-utensils"></i>Angkong's Times Kitchen
					</a>
				</h1>
				<button class="navbar-toggler ml-md-auto" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
				    aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
					<span class="navbar-toggler-icon"></span>
				</button>

				<div class="collapse navbar-collapse" id="navbarSupportedContent">

					<ul class="navbar-nav mx-auto text-center">
						<li class="nav-item active">
							<a class="nav-link" href="<?php echo $_ENV["base_url"]; ?>views/home.php">Home
								<span class="sr-only">(current)</span>
							</a>
						</li>

						<li class="nav-item">
							<a class="nav-link" href="<?php echo $_ENV["base_url"]; ?>views/menu.php">Menus</a>
						</li>


						<li class="nav-item">
							<a class="nav-link" href="<?php echo $_ENV["base_url"]; ?>views/package.php">Packages</a>
						</li>

						<li class="nav-item dropdown">
							<a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown"
							    aria-haspopup="true" aria-expanded="false">
								About
							</a>
							<div class="dropdown-menu text-lg-left text-center" aria-labelledby="navbarDropdown">
								<a class="dropdown-item" href="<?php echo $_ENV["base_url"]; ?>views/about.php">About Us</a>
								<a class="dropdown-item" href="<?php echo $_ENV["base_url"]; ?>views/feedback.php">FeedBack</a>
							</div>
						</li>

						<li class="nav-item">
							<a class="nav-link" href="<?php echo $_ENV["base_url"]; ?>views/findOrder.php"><i class="fa fa-search" aria-hidden="true"></i></a>
						</li>

					<?php if ($has_user) { ?>
							<li class="nav-item dropdown">
								<a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown"
								    aria-haspopup="true" aria-expanded="false">
									  <i class="fas fa-bell fa-fw"></i>
						               <span id="notificationNumber" class="badge badge-danger badge-counter">0</span>
								</a>
								<div id="notificationBody" class="dropdown-menu text-lg-left text-center" aria-labelledby="navbarDropdown">
								</div>
							</li>
							<li class="nav-item dropdown">
								<a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown"
								    aria-haspopup="true" aria-expanded="false">
									  <i class="fas fa-user"></i>
								</a>
								<div class="dropdown-menu text-lg-left text-center" aria-labelledby="navbarDropdown">
									<a class="dropdown-item" href="#"><?php echo $userInfo['username']; ?></a>
									<div class="dropdown-divider"></div>
									<a class="dropdown-item" href="<?php echo $_ENV["base_url"]; ?>views/profile.php">Profile</a>
									<a class="dropdown-item" href="<?php echo $_ENV["base_url"]; ?>views/trackOrder.php">Track My Order</a>
									<a class="dropdown-item" href="<?php echo $_ENV["base_url"]; ?>views/history.php">Payment History</a>
									<div class="dropdown-divider"></div>
									<a class="dropdown-item" href="#gallery" onclick="userLogout()">Logout</a>
								</div>
							</li>
						</ul>

					<?php	} else { ?>
					</ul>
						<div class="forms-w3ls-agilesm text-center mt-lg-0 mt-4">
							<ul>
								<li class="login-list-w3ls d-inline border-right pr-3 mr-3">
									<a href="<?php echo $_ENV["base_url"]; ?>views/login.php" class="text-white">Login</a>
								</li>
								<li class="login-list-w3ls d-inline">
									<a href="<?php echo $_ENV["base_url"]; ?>views/register.php" class="text-white">Register</a>
								</li>
							</ul>
						</div>
					<?php } ?>
				</div>
			</div>
		</nav>
	</header>

	<script>
		function userLogout() {
			$.ajax({
		      type: 'POST',
		      url: '<?php echo $_ENV["base_url"]; ?>controllers/controller.php',
		      data: {requestType: 'userLogout'},
		      dataType: 'JSON',
		      success: function (data) {
		        if (data.status != 'OK') {
		          swal("Error!", data.message, "warning")
		          return;
		        }

				localStorage.removeItem('menuCart');
				localStorage.removeItem('orderLine');
				localStorage.removeItem('utilityCart');
				localStorage.removeItem('payment');

		        localStorage.removeItem('user');
		        window.location = "<?php echo $_ENV["base_url"]; ?>views/home.php";
		      },
		      error: function (data) {
		        swal("Oh no!", 'Server Error', "warning")
		      }
		    })
		}
	</script>