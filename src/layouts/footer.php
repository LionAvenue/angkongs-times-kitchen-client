
	<!-- footer -->
	<footer class="pt-5">
		<div class="container py-xl-5 py-lg-3">
			<div class="row footer-grids py-4">
				<div class="col-lg-4 footer-grid text-left">
					<div class="footer-logo">
						<h2 class="mb-3">
							<a class="logo text-white" href="http://localhost/Angkongs-Times-Kitchen/index.php">
								<i class="fas fa-utensils mr-2"></i>Snacks</a>
						</h2>
					</div>
				</div>
				<div class="col-lg-2 col-6  footer-grid my-lg-0 my-4">
					<h3 class="mb-sm-4 mb-3 pb-3">Home</h3>
					<ul class="list-unstyled">
						<li>
							<a href="http://localhost/Angkongs-Times-Kitchen/index.php">Home</a>
						</li>
						<li class="my-2">
							<a class="scroll" href="#about">About</a>
						</li>
						<li class="my-2">
							<a class="scroll" href="#feedback">Feed Back</a>
						</li>
						<li>
							<a href="http://localhost/Angkongs-Times-Kitchen/views/menu.php">Menu</a>
						</li>
						<li>
							<a href="http://localhost/Angkongs-Times-Kitchen/views/Package.php">Package</a>
						</li>
					</ul>
				</div>
				<!--<div class="col-lg-2 col-6 footer-grid">
					<h3 class="mb-sm-4 mb-3 pb-3"> Company</h3>
					<ul class="list-unstyled">
						<li>
							<a href="#">Link Here</a>
						</li>
						<li class="my-2">
							<a href="#">Link Here</a>
						</li>
						<li>
							<a href="#">Link Here</a>
						</li>
					</ul>
				</div>-->
				<div class="col-lg-2 col-6 footer-grid footer-contact">
					<h3 class="mb-sm-4 mb-3 pb-3"> Contact Us</h3>
					<ul class="list-unstyled">
						<li class="mt-2">
							<a href="#">(032) 255 4907</a>
						</li>
						<li class="mt-2">
							<a href="#">Opens at 8:00am-8:00pm</a>
						</li>
						<li class="mt-2">
							<a href="#">Rodriguez, N Escario St, Cebu City, 6000 Cebu</a>
						</li>
						<li class="mt-2">
							<a href="#">angkongstimeskitchen@gmail.com</a>
						</li>
					</ul>
				</div>

				<div class="col-lg-2 col-6 footer-grid my-lg-0 my-4">
					<h3 class="mb-sm-4 mb-3 pb-3"> Navigation </h3>
					<ul class="list-unstyled">
						<li>
							<a href="https://www.google.com/maps/place/Times+Kitchen/@10.3157651,123.8877492,17z/data=!3m1!4b1!4m5!3m4!1s0x33a9994a38131213:0xcbff4728bd0b617!8m2!3d10.3157651!4d123.8899379">Visit us here!</a>
						</li>
						<li class="my-2">
							<a href="findOrder.php">Track Your Order Here!</a>
						</li>
						<!--<li>
							<a class="scroll" href="#some">Some More</a>
						</li>
						<li class="my-2">
							<a class="scroll" href="#gallery">Gallery</a>
						</li>-->
					</ul>
				</div>
			</div>
		</div>
		<!-- copyright -->
		<div class="copy_right">
			<p class="text-center text-white py-sm-4 py-3">© 2018 Snacks. All rights reserved | Design by
				<a href="http://w3layouts.com/">W3layouts</a>
			</p>

		</div>
		<!-- //copyright -->
	</footer>
	<!-- //footer -->


	<!-- Js files -->
	<!-- JavaScript -->
	<script src="<?php echo $_ENV["base_url"]; ?>js/jquery-2.2.3.min.js"></script>
	<!-- Default-JavaScript-File -->

	<!-- banner slider -->
	<script src="<?php echo $_ENV["base_url"]; ?>js/responsiveslides.min.js"></script>
	<script>
		// You can also use "$(window).load(function() {"
		$(function () {
			// Slideshow 4
			$("#slider3").responsiveSlides({
				auto: true,
				pager: true,
				nav: false,
				speed: 500,
				namespace: "callbacks",
				before: function () {
					$('.events').append("<li>before event fired.</li>");
				},
				after: function () {
					$('.events').append("<li>after event fired.</li>");
				}
			});

		});

		$(document).ready(function() {
		    $('#dataTable').DataTable();
		});
	</script>
	<!-- //banner slider -->

	<!-- gallery light box -->
	<script src="<?php echo $_ENV["base_url"]; ?>js/smoothbox.jquery2.js"></script>
	<!-- //gallery light box -->

	<!-- smooth scrolling -->
	<script src="<?php echo $_ENV["base_url"]; ?>js/SmoothScroll.min.js"></script>
	<!-- //smooth scrolling -->

	<!-- move-top -->
	<script src="<?php echo $_ENV["base_url"]; ?>js/move-top.js"></script>
	<!-- easing -->
	<script src="<?php echo $_ENV["base_url"]; ?>js/easing.js"></script>
	<!--  necessary snippets for few javascript files -->
	<script src="<?php echo $_ENV["base_url"]; ?>js/snacks.js"></script>

	<script src="<?php echo $_ENV["base_url"]; ?>js/bootstrap.js"></script>
	<!-- Necessary-JavaScript-File-For-Bootstrap -->
	<!-- //Js files -->
	<script src="<?php echo $_ENV["base_url"]; ?>vendor/datatables/jquery.dataTables.min.js"></script>
  	<script src="<?php echo $_ENV["base_url"]; ?>vendor/datatables/dataTables.bootstrap4.min.js"></script>
  	<script src="<?php echo $_ENV["base_url"]; ?>vendor/datepicker/dist/zebra_datepicker.min.js"></script>

	<script>
		if (lastSegment == 'payment.php') {
			$("#paymentStep").addClass("active");
		}

		if (lastSegment == 'utilities.php') {
			$("#utilityStep").addClass("active");
		}

		if (lastSegment == 'checkout.php') {
			$("#checkoutStep").addClass("active");
		}

		if (lastSegment == 'review.php') {
			$("#reviewStep").addClass("active");
		}
	</script>

	<script>
		let has_user = "<?php echo $has_user ?>";

		if (has_user) {
		let user_info = <?php echo json_encode($userInfo); ?>;

			$.ajax({
		      type: 'POST',
		      url: '<?php echo $_ENV["base_url"]?>controllers/controller.php',
		      data: {cust_id: user_info.cust_id, requestType: 'getNotification'},
		      dataType: 'JSON',
		      success: function (data) {
		        if (data.status != 'OK') {
		          swal("Error!", data.message, "warning")
		          return;
		        }
		        let noti_no = data.data.length
		        let template = ''
		        $('#notificationNumber').text(noti_no)

		        for (let index = 0; index < data.data.length; index++) {
		          template = `<a class="dropdown-item" href="<?php echo $_ENV["base_url"]; ?>views/order.php?trackingNo=${data.data[index].order_tracking_no}">${data.data[index].order_tracking_no} - ${data.data[index].order_status}</a>` + template
		        }

		        $('#notificationBody').append(template)
		      },
		      error: function (data) {
		        swal("Oh no!", 'Server Error', "warning")
		      }
		    })
		}
	</script>

</body>

</html>