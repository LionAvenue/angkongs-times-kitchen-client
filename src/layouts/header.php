<?php require('../env.php'); ?>
<!--
Author: W3layouts
Author URL: http://w3layouts.com
License: Creative Commons Attribution 3.0 Unported
License URL: http://creativecommons.org/licenses/by/3.0/
-->

<!DOCTYPE html>
<html lang="zxx">

<head>
	<title>Angkong's Times Kitchen</title>
	<!-- Meta tag Keywords -->
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta charset="UTF-8" />
	<meta name="keywords" content="Snacks Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template, Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design" />
	<script>
		addEventListener("load", function () {
			setTimeout(hideURLbar, 0);
		}, false);

		function hideURLbar() {
			window.scrollTo(0, 1);
		}
	</script>
	<!--// Meta tag Keywords -->

	<!-- Custom-Files -->
	<link rel="stylesheet" href="<?php echo $_ENV["base_url"]; ?>css/bootstrap.css">
	<!-- Bootstrap-Core-CSS -->
	<link rel="stylesheet" href="<?php echo $_ENV["base_url"]; ?>css/smoothbox.css" type='text/css' media="all" />
	<!-- gallery light box -->
	<link rel="stylesheet" href="<?php echo $_ENV["base_url"]; ?>css/style.css" type="text/css" media="all" />
	<!-- Style-CSS -->
	<link rel="stylesheet" href="<?php echo $_ENV["base_url"]; ?>css/fontawesome-all.css">
	<!-- Font-Awesome-Icons-CSS -->
	<!-- //Custom-Files -->

	<!-- This is what you need -->
	<script src="<?php echo $_ENV["base_url"]; ?>vendor/sweetalert/dist/sweetalert.js"></script>
	<link rel="stylesheet" href="<?php echo $_ENV["base_url"]; ?>vendor/sweetalert/dist/sweetalert.css">

	<script src="<?php echo $_ENV["base_url"]; ?>vendor/moment/moment.min.js"></script>

	<!-- Web-Fonts -->
	<link href="//fonts.googleapis.com/css?family=Lato:100,100i,300,300i,400,400i,700,700i,900,900i&amp;subset=latin-ext"
	    rel="stylesheet">
	<link href="//fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i&amp;subset=latin-ext"
	    rel="stylesheet">
	<!-- //Web-Fonts -->
	<link rel="stylesheet" type="text/css" href="<?php echo $_ENV["base_url"]; ?>vendor/datatables/datatables.min.css"/>

	<!-- Custom styles for this page -->
 	<link href="<?php echo $_ENV["base_url"]; ?>vendor/datatables/dataTables.bootstrap4.min.css" rel="stylesheet">
    <link rel="stylesheet" href="<?php echo $_ENV["base_url"]; ?>vendor/datepicker/dist/css/bootstrap/zebra_datepicker.min.css" type="text/css">

</head>


<script>
	var base_url =  window.location.href;
	var parts = base_url.split('/');
	var lastSegment = parts.pop() || parts.pop();
	lastSegment = lastSegment.split('?')[0]
</script>

<body>
	<!-- header -->
	<?php require('../src/navigations/navbar.php');?>