<?php 
require_once('hashController.php');
require_once('../email.php');
class EmployeeController
{
    public function login () {
        $conn = new database();
        $hash = new hashController();

        $email = $_POST['email'];
        $password = $hash->encryptHash($_POST['password']);

        $stmt = $conn->db()->prepare("SELECT * FROM employee WHERE `emp_email` = ? AND `emp_password` = ?");
        $stmt->execute([$email, $password]);
        $row = $stmt->fetch();

        if (empty($row)) {
            return json_encode(array('status' => 'error', 'message' => 'Invalid username or password!')); 
        }

        return json_encode(array('status' => 'OK', 'message' => 'success', 'data' => $row));
    }

    public function adminCheckEmail () {
        $conn = new database();
        $email = $_POST['email'];

        $stmt = $conn->db()->prepare("SELECT * FROM `employee` WHERE `emp_email` = ?");
        $stmt->execute([$email]);
        $row = $stmt->fetch();  

        if (empty($row)) {
            return json_encode(array('status' => 'error', 'message' => 'No User found')); 
        }

        $this->adminGenerateKey($row);
        return json_encode(array('status' =>'OK' , 'message' =>  'success', 'data' => $row));   
    }

    public function adminGenerateKey ($employee) {
        $key = 'KEY'.substr(md5(uniqid(json_encode($employee), true)), 0, 8);
        $conn = new database();

        $stmt = $conn->db()->prepare("UPDATE `employee` SET `key`= ? WHERE `emp_id` = ?");
        $stmt->execute([$key, $employee['emp_id']]);

        $email = new emailController();

        $email->adminSendEmail($employee, $key);

        return true;
    }

    public function adminCheckKey () {
        $key = $_POST['key'];
        $conn = new database();

        $stmt = $conn->db()->prepare("SELECT * FROM `employee` WHERE `key` = ?");
        $stmt->execute([$key]);
        $row = $stmt->fetch();  

        if (empty($row)) {
            return json_encode(array('status' => 'error', 'message' => 'Invalid Key!')); 
        }

        return json_encode(array('status' =>'OK' , 'message' =>  'success'));
    }

    public function adminChangePassword () {
        $conn = new database();
        $hash = new hashController();
        $emp_id = $_POST['emp_id'];
        $password = $hash->encryptHash($_POST['password']);

        $stmt = $conn->db()->prepare("UPDATE `employee` SET `emp_password`= ? WHERE `emp_id` = ?");
        $stmt->execute([$password, $emp_id]);

        return json_encode(array('status' =>'OK' , 'message' =>  'success'));
    }
}

?>