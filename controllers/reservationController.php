<?php 
require_once('../database/database.php');
require_once('../controllers/hashController.php');
require('../env.php');

class reservationController
{
	public function userReservation () {
		$conn = new database();
		$payment = $_POST['payment'];
		$cust_id = $_POST['cust_id'];
		$order_status = 'pending';
		$package_id = NULL;
		$today = date('Y-m-d');

		$combination = array('cust_id' => $cust_id, 'payment', $payment);
		$tracking_number = 'ATK'.substr(md5(uniqid(json_encode($combination), true)), 0, 8);

		if ($payment['orderType'] == 'package') {
			$package_id = $payment['package_id'];
		}

		$stmt = $conn->db()->prepare("INSERT INTO `order_tbl` (`cust_id`, `order_date`, `order_status`, `order_payment`, `order_type`, `package_id`, `address`, `city`, `created_at`, `order_tracking_no`) VALUES (?, ?, ?, ? ,?, ?, ?, ?, ?, ?)");
		$stmt->execute([$cust_id, $payment['eventDate'], $order_status, $payment['total'], $payment['orderType'], $package_id, $payment['address'], $payment['city'], $today, $tracking_number]);

		if ($payment['orderType'] == 'menu') {
			$this->orderLineReservation();
		}

		$this->serviceReservation();
		$this->createPayment();
		return json_encode(array('status' => 'OK', 'message' => 'success'));
	}

	public function createPayment () {
		$conn = new database();		
		$order_id = $this->getReservationLastInsertId();
		$payment = $_POST['payment'];
		$cust_id = $_POST['cust_id'];
		$branch = '';
		$referenceNumber = '';
		$package_image = null;

		if (isset($payment['referenceImage'])) {
			$package_image = $payment['referenceImage'];
		}
		
		if (isset($_POST['payment']['referenceNumber'])) {
			$referenceNumber = $_POST['payment']['referenceNumber'];
		}

		if (isset($_POST['payment']['remittanceBranch'])) {
			$branch = $_POST['payment']['remittanceBranch'];
		}

		$stmt = $conn->db()->prepare("INSERT INTO `payment_logs` (`order_id`, `cust_id`, `subtotal`, `rem_bal`, `payment_type`, `remittance_provider`, `reference_number`, `reference_img`) VALUES (?, ?, ?, ? ,?, ?, ?, ?)");
		$stmt->execute([$order_id, $cust_id, $payment['subTotal'], $payment['total'], $payment['method'], $branch, $referenceNumber, $package_image]);

		return true;
	}

	public function orderLineReservation () {
		$conn = new database();
		$order_id = $this->getReservationLastInsertId();

		if (isset($_POST['orderLine'])) {
			$order_line = $_POST['orderLine'];

			foreach ($order_line as $row) {	
				$stmt = $conn->db()->prepare("INSERT INTO `orderline_tbl` (`order_id`, `food_id`, `quantity`, `size`, `price_size`) VALUES (?, ?, ?, ? ,?)");
				$stmt->execute([$order_id, $row['food_id'], $row['quantity'], $row['size'], $row['price']]);
			}
		}

		return true;
	}

	public function serviceReservation () {
		$conn = new database();
		$order_id = $this->getReservationLastInsertId();

		if (isset($_POST['utility'])) {
			$utility = $_POST['utility'];

			foreach ($utility as $row) {
				$stmt = $conn->db()->prepare("INSERT INTO `services` (`order_id`, `utility_id`, `quantity`) VALUES (?, ?, ?)");
				$stmt->execute([$order_id, $row['utility_id'], $row['quantity']]);
				$this->updateUtilityQuantity($row['utility_id'], $row['quantity']);
			}
		}

		return true;
	}

	public function updateUtilityQuantity ($id, $quantity) {
		$conn = new database();

		$stmt = $conn->db()->prepare("UPDATE `utilities` SET `utility_total` = `utility_total` - ? WHERE `utility_id` = ?");
		$stmt->execute([$quantity, $id]);

		return true;
	}

	public function customerReservation ($user_id) {
		$conn = new database();

		$stmt = $conn->db()->prepare("SELECT * FROM `order_tbl` WHERE `cust_id` = ?");
		$stmt->execute([$user_id]);
		$rows = $stmt->fetchAll();

		return $rows;
	}

	public function viewOrder () {
		$conn = new database();
		$hash = new hashController();
		$hash_id = $_POST['order_id'];
		$order_id = $hash->decryptHash($hash_id);
		$menu = array();

		$stmt = $conn->db()->prepare("SELECT * FROM `order_tbl` WHERE `order_id` = ?");
		$stmt->execute([$order_id]);
		$row = $stmt->fetch();

		$services = $this->getOrderService($order_id);
		$payment = $this->getOrderPayment($order_id);

		if (isset($row['package_id'])) {
			$menu = $this->getOrderPackage($row['package_id']);
		} else {
			$menu = $this->getOrderLine($order_id);
		}

    	return json_encode(array('status' => 'OK', 'message' => 'success', 'order' => $row, 'service' => $services, 'menu' => $menu, 'payment' => $payment));
	}

	public function getOrderLine ($order_id) {
		$conn = new database();

		$stmt = $conn->db()->prepare("SELECT * FROM `orderline_tbl` INNER JOIN `food` ON `orderline_tbl`.food_id = `food`.food_id WHERE `order_id` = ?");
		$stmt->execute([$order_id]);
		$rows = $stmt->fetchAll();

    	return $rows;	
	}

	public function getOrderService ($order_id) {
		$conn = new database();
		
		$stmt = $conn->db()->prepare("SELECT * FROM `services` INNER JOIN `utilities` ON `services`.utility_id = `utilities`.utility_id WHERE `order_id` = ?");
		$stmt->execute([$order_id]);
		$rows = $stmt->fetchAll();

		return $rows;
	}

	public function getOrderPackage ($package_id) {
		$conn = new database();
		
		$stmt = $conn->db()->prepare("SELECT * FROM `package_item` INNER JOIN `food` ON `package_item`.food_id = `food`.food_id WHERE `package_id` = ?");
		$stmt->execute([$package_id]);
		$rows = $stmt->fetchAll();

		return $rows;
	}

	public function getOrderPayment ($order_id) {
		$conn = new database();
		
		$stmt = $conn->db()->prepare("SELECT * FROM `payment_logs` WHERE `order_id` = ?");
		$stmt->execute([$order_id]);
		$rows = $stmt->fetch();

		return $rows;
	}

	public function cancelReservation () {
		$conn = new database();
		$hash = new hashController();
		$hash_id = $_POST['order_id'];
		$order_id = $hash->decryptHash($hash_id);

		$stmt = $conn->db()->prepare("UPDATE `order_tbl` SET `order_status` = ? WHERE `order_id` = ?");
		$stmt->execute(['cancelled', $order_id]);

		return json_encode(array('status' => 'OK', 'message' => 'success'));
	}

	public function getOrderIdByTracking ($tracking_number) {
	    $conn = new database();

	    $stmt = $conn->db()->prepare("SELECT * FROM `order_tbl` INNER JOIN `customer` ON `order_tbl`.cust_id = `customer`.cust_id WHERE `order_tracking_no` = ?");
	    $stmt->execute([$tracking_number]);
	    $rows = $stmt->fetch();

	    return $rows['order_id'];
  	}

	public function getReservationLastInsertId () {
		$conn = new database();
		$stmt = $conn->db()->prepare("SELECT `order_id` FROM `order_tbl` ORDER BY order_id DESC LIMIT 1");
		$stmt->execute();
		$row = $stmt->fetch();
		return $row['order_id'];		
	}

	public function getCustomerPaymentHistory ($user_id) {
		$conn = new database();

		$stmt = $conn->db()->prepare("SELECT * FROM `payment_logs` WHERE `cust_id` = ?");
		$stmt->execute([$user_id]);
		$rows = $stmt->fetchAll();

		return $rows;
	}

	public function checkTrackingNo () {
	    $conn = new database();
	    $tracking_number = $_POST['tracking_number'];

	    $stmt = $conn->db()->prepare("SELECT * FROM `order_tbl` WHERE `order_tracking_no` = ?");
	    $stmt->execute([$tracking_number]);
	    $rows = $stmt->fetch();

	    if (empty($rows)) {
			return json_encode(array('status' => 'error', 'message' => 'Invalid Tracking Order!')); 
    	}
	    

	    return json_encode(array('status' => 'OK', 'message' => 'success'));
  	}

  	public function uploadReferenceImage () {
		$conn = new database();
		$package_image = "";

		if (isset($_FILES['packageFile'])) {		
			$src = $_FILES['packageFile']['tmp_name'];
			$menuFilePath = "../images/reference/".$_FILES['packageFile']['name'];
			move_uploaded_file($src, $menuFilePath);
			$package_image = $_FILES['packageFile']['name']; 
		}

		return json_encode(array('status' => 'OK', 'message' => 'success'));
	}
}

 ?>