<?php
require_once('../database/database.php');
require_once('menuController.php');

class PackageController
{
	public function getPackageList ($type = null) {
		$conn = new database();
		$dataType = isset($_POST['dataType']) ? $_POST['dataType'] : null;


		if ($type) {
			$stmt = $conn->db()->prepare("SELECT * FROM `package` WHERE `is_delete` = '0' AND  `category` = ?");
	    	$stmt->execute([$type]);
	    	$rows = $stmt->fetchAll();
		} else {
			$stmt = $conn->db()->prepare("SELECT * FROM `package` WHERE `is_delete` = '0'");
	    	$stmt->execute();
	    	$rows = $stmt->fetchAll();
		}

    	if ($dataType == 'JSON') {
    		return json_encode(array('status' => 'OK', 'message' => 'success', 'data' => $rows));
    	}

		return $rows;
	}

	public function getPackage () {
		$conn = new database();

		$package_id = $_POST['package_id'];

    	$stmt = $conn->db()->prepare("SELECT * FROM package WHERE `package_id` = ?");
    	$stmt->execute([$package_id]);
    	$row = $stmt->fetch();

    	if (empty($row)) {
			return json_encode(array('status' => 'error', 'message' => 'Package not found'));
    	}
		
		$package_item = $this->getPackageItem();
    	return json_encode(array('status' => 'OK', 'message' => 'success', 'package' => $row, 'package_item' => $package_item));
	}

	public function getPackageItem () {
		$conn = new database();
    	$package_id = $_POST['package_id'];

    	$stmt = $conn->db()->prepare("SELECT * FROM package_item INNER JOIN `food` ON `package_item`.food_id = `food`.food_id WHERE `package_id` = ?");
    	$stmt->execute([$package_id]);
    	$rows = $stmt->fetchAll();

    	return $rows;
    }

    public function viewPackageItem ($id) {
    	$conn = new database();
		$food_id = $id;

    	$stmt = $conn->db()->prepare("SELECT * FROM package_item WHERE `food_id` = ?");
    	$stmt->execute([$food_id]);
    	$rows = $stmt->fetchAll();

		return $rows;
    }
}

 ?>