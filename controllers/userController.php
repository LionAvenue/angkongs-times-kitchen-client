<?php
require_once('../database/database.php');
require_once('hashController.php');
require('../session/sessionController.php');
require_once('../email.php');

class userController
{
	public function userlogin () {
		$conn = new database();
		$hash = new hashController();

		$username = $_POST['username'];
		$password = $hash->encryptHash($_POST['password']);

    	$stmt = $conn->db()->prepare("SELECT * FROM `customer` WHERE `username` = ? AND `password` = ?");
    	$stmt->execute([$username, $password]);
    	$row = $stmt->fetch();

    	if (empty($row)) {
			return json_encode(array('status' => 'error', 'message' => 'Invalid username or password!')); 
    	}

    	session_start();
    	$_SESSION['user'] = $row;

    	return json_encode(array('status' => 'OK', 'message' => 'success', 'data' => $row));
	}

	public function userRegister () {
		$conn = new database();
		$hash = new hashController();

		$username = $_POST['username'];
		$firstname = $_POST['firstname'];
		$middlename = $_POST['middlename'];
		$lastname = $_POST['lastname'];
		$email = $_POST['email'];
		$address = $_POST['address'];
		$mobilenumber = $_POST['mobilenumber'];
		$password = $hash->encryptHash($_POST['password']);

		if($this->checkUsernameExist()){
			return json_encode(array('status' => 'error', 'message' => 'Username Already Exist!'));			
		}

		if($this->checkEmailExist()){
			return json_encode(array('status' => 'error', 'message' => 'Email Already Exist!'));			
		}

		$stmt = $conn->db()->prepare("INSERT INTO `customer` (`cust_fname`, `cust_lname`, `cust_mi`, `user_add`, `cust_cn`, `cust_email`, `username`, `password`) VALUES (?, ?, ?, ?, ?, ?, ?, ?)");
    	$stmt->execute([$firstname, $lastname, $middlename, $address, $mobilenumber, $email, $username, $password]);


    	return json_encode(array('status' => 'OK', 'message' => 'success'));
	}

	public function userUpdate () {
		$conn = new database();
		$hash = new hashController();

		$username = $_POST['username'];
		$firstname = $_POST['firstname'];
		$middlename = $_POST['middlename'];
		$lastname = $_POST['lastname'];
		$email = $_POST['email'];
		$address = $_POST['address'];
		$mobilenumber = $_POST['mobilenumber'];
		$password = $hash->encryptHash($_POST['password']);
		$custId = $_POST['custId'];

		// if($this->checkUsernameExist()){
		// 	return json_encode(array('status' => 'error', 'message' => 'Username Already Exist!'));			
		// }

		// if($this->checkEmailExist()){
		// 	return json_encode(array('status' => 'error', 'message' => 'Email Already Exist!'));			
		// }

		$userProfile = array('cust_fname' => $firstname, 'cust_mi' => $middlename, 'cust_lname' => $lastname , 'user_add' => $address, 'cust_cn' => $mobilenumber, 'cust_email' => $email, 'username' => $username, 'password' => $password, 'cust_id' => $custId);

		session_start();
    	$_SESSION['user'] = $userProfile;


		$stmt = $conn->db()->prepare("UPDATE `customer` SET `cust_fname`= ?, `cust_mi`= ?,`cust_lname`= ?,`user_add`= ?,`cust_cn`= ?,`cust_email`= ?, `username`= ?,`password`= ? WHERE `cust_id` = ?");
		$stmt->execute([$firstname, $middlename, $lastname, $address, $mobilenumber, $email, $username, $password, $custId]);
		return json_encode(array('status' => 'OK', 'message' => 'success'));
	}

	public function userLogout () {
		$session = new sessionController(); 
		$session->destroy();
		return json_encode(array('status' => 'OK', 'message' => 'success'));
	}

	public function createFeedback() {

		$conn = new database();

		$fmsg = $_POST['fMsg'];
		$fRate = $_POST['fRate'];
		$fname =$_POST['fName'];
		$date = date("Y-m-d");

	//	$feedback_date = date

		//PDO QUERY

		$stmt = $conn->db()->prepare("INSERT INTO `feedback`(`user_name`, `rate`,`feedback_message`, `date_sent`) VALUES (?,?,?,?) ");
		$stmt->execute([$fname , $fRate, $fmsg, $date]);


		return json_encode(array('status' =>'OK' , 'message' =>  'success'));

	}

	public function getFeedback() {
		$conn = new database();

		$stmt = $conn->db()->prepare("SELECT * FROM `feedback` ORDER BY `date_sent` DESC LIMIT 3");
		$stmt->execute();
		$rows = $stmt->fetchAll();

		return json_encode(array('status' =>'OK' , 'message' =>  'success', 'data' => $rows));
	}

	public function checkEmail () {
		$conn = new database();
		$email = $_POST['email'];

		$stmt = $conn->db()->prepare("SELECT * FROM `customer` WHERE `cust_email` = ?");
		$stmt->execute([$email]);
		$row = $stmt->fetch();	

    	if (empty($row)) {
			return json_encode(array('status' => 'error', 'message' => 'No User found')); 
    	}

    	$this->generateKey($row);
		return json_encode(array('status' =>'OK' , 'message' =>  'success', 'data' => $row));	
	}
	

	public function generateKey ($user) {
		$key = 'KEY'.substr(md5(uniqid(json_encode($user), true)), 0, 8);
		$conn = new database();

		$stmt = $conn->db()->prepare("UPDATE `customer` SET `key`= ? WHERE `cust_id` = ?");
		$stmt->execute([$key, $user['cust_id']]);

		$email = new emailController();

		$email->sendEmail($user, $key);

		return true;
	}

	public function checkKey () {
		$key = $_POST['key'];
		$conn = new database();

		$stmt = $conn->db()->prepare("SELECT * FROM `customer` WHERE `key` = ?");
		$stmt->execute([$key]);
		$row = $stmt->fetch();	

    	if (empty($row)) {
			return json_encode(array('status' => 'error', 'message' => 'Invalid Key!')); 
    	}

		return json_encode(array('status' =>'OK' , 'message' =>  'success'));
	}

	public function changePassword () {
		$conn = new database();
		$hash = new hashController();
		$cust_id = $_POST['user_id'];
		$password = $hash->encryptHash($_POST['password']);

		$stmt = $conn->db()->prepare("UPDATE `customer` SET `password`= ? WHERE `cust_id` = ?");
		$stmt->execute([$password, $cust_id]);

		return json_encode(array('status' =>'OK' , 'message' =>  'success'));
	}

	public function checkUsernameExist (){
		$conn = new database();
		$username = $_POST['username'];

		$stmt = $conn->db()->prepare("SELECT * FROM `customer` WHERE `username` = ?");
		$stmt->execute([$username]);
		$row = $stmt->fetch();

		if(empty($row)){
			return false;
		}else{
			return true;
		}
	}

	public function checkEmailExist (){
		$conn = new database();
		$email = $_POST['email'];

		$stmt = $conn->db()->prepare("SELECT * FROM `customer` WHERE `cust_email` = ?");
		$stmt->execute([$email]);
		$row = $stmt->fetch();

		if(empty($row)){
			return false;
		}else{
			return true;
		}
	}

}

 ?>