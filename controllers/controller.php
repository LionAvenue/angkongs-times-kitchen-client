<?php 
require_once('userController.php');
require_once('packageController.php');
require_once('reservationController.php');
require_once('employeeController.php');
require_once('notificationController.php');

$user = new userController();
$package = new packageController();
$reservation = new reservationController();
$employee = new employeeController();
$notification = new notificationController();

if (isset($_POST['requestType']) || isset($_GET['requestType'])) {

	if ($_SERVER['REQUEST_METHOD'] === 'POST') {
		$request = $_POST['requestType'];
	} else {
		$request = $_GET['requestType'];
	}

	if ($request == 'userLogin') {
		echo $user->userLogin();
		return;
	}

	if ($request == 'userRegister') {
		echo $user->userRegister();
		return;
	}

	if ($request == 'userLogout') {
		echo $user->userLogout();
		return;
	}

	if ($request == 'userReservation') {
		echo $reservation->userReservation();
		return;
	}

	if ($request == 'searchOrder') {
		echo $reservation->checkTrackingNo();
		return;
	}

	if ($request == 'userFeedback') {
		echo $user->userFeedback();
		return;
	}

	if ($request == 'getPackage') {
		echo $package->getPackage();
		return;
	}

	if ($request == 'getOrderLine') {
		echo $reservation->viewOrder();
		return;
	}

	if ($request == 'employeeLogin') {
		echo $employee->login();
		return;
	}

	if ($request == 'adminCheckEmail') {
		echo $employee->adminCheckEmail();
		return;
	}

	if ($request == 'adminCheckKey') {
		echo $employee->adminCheckKey();
		return;
	}

	if ($request == 'adminForgotPassword') {
		echo $employee->adminForgotPassword();
		return;
	}

	if ($request == 'adminChangePassword') {
		echo $employee->adminChangePassword();
		return;
	}

	if ($request == 'userUpdate') {
		echo $user->userUpdate();
		return;
	}

	if ($request == 'createFeedback') {
		echo $user->createFeedback();
		return;
	}


	if ($request == 'cancelOrder') {
		echo $reservation->cancelReservation();
		return;
	}

	if ($request == 'getFeedback') {
		echo $user->getFeedback();
		return;
	}

	if ($request == 'getNotification') {
		echo $notification->getNotification();
		return;
	}

	if ($request == 'updateCustomerNotfi') {
		echo $notification->updateCustomerNotfi();
		return;
	}

	if ($request == 'checkEmail') {
		echo $user->checkEmail();
		return;
	}

	if ($request == 'checkKey') {
		echo $user->checkKey();
		return;
	}

	if ($request == 'changePassword') {
		echo $user->changePassword();
		return;
	}

	if ($request == 'uploadReferenceImage') {
		echo $reservation->uploadReferenceImage();
		return;
	}

	echo json_encode(array('status' => 'error', 'message' => '404 Not Found'));
	// {status: error, message: 404 not found}
}

?>