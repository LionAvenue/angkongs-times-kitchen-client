<?php 
require_once('../database/database.php');
require_once('../controllers/hashController.php');

class NotificationController
{
	public function getNotification () {
		$conn = new database();
		$cust_id = $_POST['cust_id'];

		$stmt = $conn->db()->prepare("SELECT * FROM `order_tbl` WHERE `order_status` <> 'pending' = 1 AND `cust_id` = ?");
    	$stmt->execute([$cust_id]);
    	$rows = $stmt->fetchAll();

		return json_encode(array('status' => 'OK', 'message' => 'success', 'data' => $rows));	
	}

	public function updateOrderStatus () {
		$conn = new database();
		$order_id = $_POST['order_id'];

		$stmt = $conn->db()->prepare("UPDATE `order_tbl` SET `notif_status` = ? WHERE `order_id` = ?");
    	$stmt->execute([1, $order_id]);

		return json_encode(array('status' => 'OK', 'message' => 'success'));
	}

	public function updateCustomerNotfi () {
		$conn = new database();
		$hash = new hashController();
		$order_id = $hash->decryptHash($_POST['order_id']);

		$stmt = $conn->db()->prepare("UPDATE `order_tbl` SET `cust_notif_status` = 0 WHERE `order_id` = ?");
		$stmt->execute([$order_id]);

		return json_encode(array('status' => 'OK', 'message' => 'success'));
	}
}

 ?>