<?php 
require_once('../database/database.php');

class MenuController
{
	public function getMenuList ($type = null) {
		$conn = new database();


		if ($type) {
			$stmt = $conn->db()->prepare("SELECT * FROM `food` WHERE `is_delete` = '0' AND `category` = ?");
	    	$stmt->execute([$type]);
	    	$rows = $stmt->fetchAll();
		} else {
			$stmt = $conn->db()->prepare("SELECT * FROM `food` WHERE `is_delete` = '0'");
	    	$stmt->execute();
	    	$rows = $stmt->fetchAll();
		}
		

		return $rows;
	}

	public function getMenu ($foodId = null) {
		$conn = new database();

		$food_id = isset($_POST['id']) ? $_POST['id'] : $foodId;

    	$stmt = $conn->db()->prepare("SELECT * FROM food WHERE `food_id` = ?");
    	$stmt->execute([$food_id]);
    	$row = $stmt->fetch();

    	if (empty($row)) {
			return json_encode(array('status' => 'error', 'message' => 'Menu not found'));
    	}

    	return json_encode(array('status' => 'OK', 'message' => 'success', 'data' => $row));
	}
}

?>