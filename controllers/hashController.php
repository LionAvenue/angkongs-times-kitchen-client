<?php 

class HashController
{

static $output = false;
static $encrypt_method = "AES-256-CBC";
static $secret_key = 'atkDev';
static $secret_iv = 'atkDev123';

	public function key () {
		return hash('sha256', HashController::$secret_key);
	}

	public function iv () {
		 return substr(hash('sha256', HashController::$secret_iv), 0, 16);
	}

	public function encryptHash ($string) {
		$output = openssl_encrypt($string, HashController::$encrypt_method, $this->key(), 0, $this->iv());
    	$output = base64_encode($output);
    	return $output;
	}

	public function decryptHash ($string) {
		$output = openssl_decrypt(base64_decode($string), HashController::$encrypt_method, $this->key(), 0, $this->iv());
		return $output;
	}
}

?>