<?php
require_once('../database/database.php');


class categoryController
{
	public function getCategoryList ($type = null) {
		$conn = new database();
		$dataType = isset($_POST['dataType']) ? $_POST['dataType'] : null;
		$category = $type ? $type : 'menu';

		if (isset($_POST['categoryType'])) {
			$category = $_POST['categoryType'];
		}

		$stmt = $conn->db()->prepare("SELECT * FROM `category` WHERE `is_delete` = '0' AND category_type = ?");
    	$stmt->execute([$category]);
    	$rows = $stmt->fetchAll();

    	if ($dataType == 'JSON') {
    		return json_encode(array('status' => 'OK', 'message' => 'success', 'data' => $rows));
    	}

		return $rows;
	}

	public function getDistinctCategory ($type) {
		$conn = new database();


		$stmt = $conn->db()->prepare("SELECT DISTINCT `category_name` FROM `category` WHERE category_type = ? AND `is_delete` = '0' ");
    	$stmt->execute([$type]);
    	$rows = $stmt->fetchAll();

    	return $rows;
	}

	public function checkIs_delete ($type) {
		$conn = new database();


		$stmt = $conn->db()->prepare("SELECT `is_delete` FROM `category` WHERE category_type = ?");
    	$stmt->execute([$type]);
    	$rows = $stmt->fetchAll();

    	return $rows;
	}
}

 ?>