<?php require('../src/layouts/header.php');?>
<?php 
  $user_id = null; 
  if (isset($_GET['id'])) {
    $user_id = $_GET['id'];
  }
?>
	<div class="py-5" id="login">
		<div class="container py-xl-5 py-lg-3">
			<div class="row pt-lg-5 justify-content-md-center">
				<div class="col-sm-6 col-sm-offset-3 address-left wow agile fadeInLeft animated mt-lg-0 mt-5" data-wow-delay=".5s">
					<div class="address-grid p-sm-5 p-4">
						<h4 class="wow fadeIndown animated mb-3" data-wow-delay=".5s">Reset Password</h4>
						<form>
						  <div class="form-group">
                <label for="exampleInputPassword1">New Password</label>
                <input type="password" class="form-control" id="updatePassword1" placeholder="Password">
              </div>
						  <div class="form-group">
						    <label for="exampleInputPassword1">Retype New Password</label>
						    <input type="password" class="form-control" id="updatePassword2" placeholder="Password">
						  </div>
						  <button type="button" class="btn btn-primary" onclick="forgotPassword()">Submit</button>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>

	<style scope>
		.navbar {
		  /*background-color: #A1887F !important;*/
		  background-image: url("<?php echo $_ENV["base_url"]; ?>images/1.jpg") !important;
		}
	</style>


<?php require('../src/layouts/footer.php');?>
<script>
let user_id = <?php echo $user_id?>;
if (!user_id) {
      window.location.href = `<?php echo $_ENV["base_url"]?>views/email.php`;
}


function forgotPassword () {
	let password1 = $('#updatePassword1').val()
	let password2 = $('#updatePassword2').val()

    if (password1 != password2) {
      swal("Hey!", 'The password you enter does not match!', "warning")
      return;
    }

    $.ajax({
      type: 'POST',
      url: '<?php echo $_ENV["base_url"]; ?>controllers/controller.php',
      data: {user_id: user_id, password: password1, requestType: 'changePassword'},
      dataType: 'JSON',
      success: function (data) {
        if (data.status != 'OK') {
          swal("Error!", data.message, "warning")
          return;
        }

        swal({
          title: "Nice!",
          text: "Your password has changed!",
          type: "success",
          confirmButtonClass: "btn-success",
          closeOnConfirm: false
        },
        function(isConfirm){
          if (isConfirm) {
           window.location.href = "<?php echo $_ENV["base_url"]; ?>views/login.php";  
          }
        });       
      },
      error: function (data) {
        swal("Oh no!", 'Server Error', "warning")
      }
    })
}
</script>
