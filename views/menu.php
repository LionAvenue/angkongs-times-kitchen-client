<?php
require('../src/layouts/header.php'); 
require('../controllers/menuController.php');
require('../controllers/categoryController.php'); 
$menu = new menuController();
$category = new categoryController();
$menuCategory = $category->getDistinctCategory('menu');

if (isset($_GET['menu'])) {
	$menuList = $menu->getMenuList($_GET['menu']);
	$menuListObject = json_encode($menuList);
} else {
	$menuList = $menu->getMenuList();
	$menuListObject = json_encode($menuList);
}
?>
	<div class="py-5" id="login">
		<div class="container py-xl-5 py-lg-3">
			<div class="row pt-lg-5 justify-content-md-center">
				<div class="col-sm-12 col-sm-offset-3 address-left wow agile fadeInLeft animated mt-lg-0 mt-5" data-wow-delay=".5s">
					<div class="address-grid p-sm-5 p-4">

					   <!-- Page Content -->
					  <div class="container">

					    <div class="row">

					      <div class="col-lg-3">

					        <h1 class="display-4">Menu</h1>
					        <div class="list-group">
					        <a href="<?php echo $_ENV["base_url"]; ?>views/menu.php" class="list-group-item">All</a>
					        <?php foreach ($menuCategory as $row) { ?>
					        	<a href="<?php echo $_ENV["base_url"]; ?>views/menu.php?menu=<?php echo $row['category_name']; ?>" class="list-group-item"><?php echo $row['category_name']; ?></a>
					        <?php } ?>
					        </div>

					      </div>
					      <!-- /.col-lg-3 -->

					      <div class="col-lg-9">
					      	<a href="<?php echo $_ENV["base_url"]; ?>views/cart.php" class="text-right"><h5>Cart <i class="fa fa-shopping-cart"></i> | <span id="cartTotal" class="badge badge-primary"></span></h5></a>
					        <div id="carouselExampleIndicators" class="carousel slide my-4" data-ride="carousel">
					          <ol class="carousel-indicators">
					            <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
					            <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
					            <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
					          </ol>
					          <div class="carousel-inner" role="listbox">
					            <div class="carousel-item active">
					              <img class="d-block img-fluid" src="<?php echo $_ENV["base_url"]; ?>images/cover1.jpg" alt="First slide">
					            </div>
					            <div class="carousel-item">
					              <img class="d-block img-fluid" src="<?php echo $_ENV["base_url"]; ?>images/cover2.jpg" alt="Second slide">
					            </div>
					            <div class="carousel-item">
					              <img class="d-block img-fluid" src="<?php echo $_ENV["base_url"]; ?>images/cover3.png" alt="Third slide">
					            </div>
					          </div>
					          <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
					            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
					            <span class="sr-only">Previous</span>
					          </a>
					          <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
					            <span class="carousel-control-next-icon" aria-hidden="true"></span>
					            <span class="sr-only">Next</span>
					          </a>
					        </div>

					        <div class="row">

					        <?php foreach ($menuList as $row ) { ?>
						        <div class="col-lg-4 col-md-6 mb-4">
						            <div class="card h-100">
						              <a href="#"><img class="card-img-top" src="<?php echo $_ENV["base_url_admin"] .'/img/menu_img/'. $row['food_image']?>" alt="" height="150"></a>
						              <div class="card-body">
						                <h4 class="card-title">
						                  <a href="#"><?php echo $row['food_name']; ?></a>
						                </h4>
						                <p class="card-text">
							                XS <?php echo $row['price_xsmall']; ?> <br>
							                X <?php echo $row['price_small']; ?> <br>
							                M <?php echo $row['price_medium']; ?> <br>
							                L <?php echo $row['price_large']; ?> <br>	
						                </p>
						                <hr>
						                <p class="card-text">
						                	<?php echo $row['food_desc']; ?>
						                </p>
						              </div>
						             <?php if($has_user) { ?>
						             	<?php if($row['is_available'] == 0 || $row['is_available'] == NULL) { ?>
						             	<div class="card-footer text-center">
							             	<div class="btn-group" role="group" aria-label="Basic example">
											  <button type="button" id="addCartBtn<?php echo $row['food_id'].'xs'; ?>" class="btn btn-success" onclick="addToCart(<?php echo $row['food_id']; ?>, 'extra small', <?php echo $row['price_xsmall']; ?>)">XS</button>
											  <button type="button" id="rmvCartBtn<?php echo $row['food_id'].'xs'; ?>" class="btn btn-danger" style="display: none" onclick="removeToCart(<?php echo $row['food_id']; ?>, 'extra small')">XS</button>

											  <button type="button" id="addCartBtn<?php echo $row['food_id'].'s'; ?>" class="btn btn-success" onclick="addToCart(<?php echo $row['food_id']; ?>, 'small', <?php echo $row['price_small']; ?>)">S</button>
											  <button type="button" id="rmvCartBtn<?php echo $row['food_id'].'s'; ?>" class="btn btn-danger" style="display: none" onclick="removeToCart(<?php echo $row['food_id']; ?>, 'small')">S</button>
											  
											  <button type="button" id="addCartBtn<?php echo $row['food_id'].'m'; ?>" class="btn btn-success" onclick="addToCart(<?php echo $row['food_id']; ?>, 'medium', <?php echo $row['price_medium']; ?>)">M</button>
											  <button type="button" id="rmvCartBtn<?php echo $row['food_id'].'m'; ?>" class="btn btn-danger" style="display: none" onclick="removeToCart(<?php echo $row['food_id']; ?>, 'medium')">M</button>
											  
											  <button type="button" id="addCartBtn<?php echo $row['food_id'].'l'; ?>" class="btn btn-success" onclick="addToCart(<?php echo $row['food_id']; ?>, 'large', <?php echo $row['price_large']; ?>)">L</button>
											  <button type="button" id="rmvCartBtn<?php echo $row['food_id'].'l'; ?>" class="btn btn-danger" style="display: none" onclick="removeToCart(<?php echo $row['food_id']; ?>, 'large')">L</button>
											</div>
						              	</div>
						              	<?php } else { ?>
						              		<div class="card-footer text-center">
						              			<button class="btn btn-danger btn-block" disabled="true">Out Of Stock</button>
						              		</div>
						              	<?php } ?>
						             <?php } ?>
						            </div>
						          </div>
					        <?php } ?>

					        </div>
					        <!-- /.row -->

					      </div>
					      <!-- /.col-lg-9 -->

					    </div>
					    <!-- /.row -->

					  </div>

					</div>
				</div>
			</div>
		</div>
	</div>

	<style scope>
		.navbar {
		  /*background-color: #A1887F !important;*/
		  background-image: url("<?php echo $_ENV["base_url"]; ?>images/1.jpg") !important;
		}
	</style>


<?php require('../src/layouts/footer.php');?>
<script>
	let menuList = <?php echo $menuListObject; ?>;
	let menuCart = localStorage.getItem('menuCart') || [];
	let cartTotal = menuCart ? menuCart.length : 0
	let user = localStorage.getItem('user') || [];
	let size_template = {
		'extra small': 'xs',
		'small': 's',
		'medium': 'm',
		'large': 'l'
	}


	if (typeof menuCart == 'string') {
		menuCart = JSON.parse(menuCart)

		for (let index = 0; index < menuCart.length; index++) {
			$(`#addCartBtn${menuCart[index].food_id}${size_template[menuCart[index].size]}`).hide()
			$(`#rmvCartBtn${menuCart[index].food_id}${size_template[menuCart[index].size]}`).show()
		}

		$('#cartTotal').text(menuCart.length)
	}

	function addToCart (menu_id, size, price) {
		let menu = menuList.find(function (list) {
			return list.food_id == menu_id;
		})

		let exist = false
		let cart = {}

		for (let index = 0; index < menuCart.length; index ++) {
		    if (menuCart[index].food_id === menu.food_id && menuCart[index].size === size) {
		     	exist = true
		     	break;
		    }
		}

		if (!exist) {
			cart.size = size
			cart.price_size = price
			menuCart.push({...menu, ...cart})
			$(`#addCartBtn${menu_id}${size_template[size]}`).hide()
			$(`#rmvCartBtn${menu_id}${size_template[size]}`).show()
			localStorage.setItem("menuCart", JSON.stringify(menuCart));
		}

		$('#cartTotal').text(menuCart.length)
	}

	function removeToCart (menu_id, size) {
		let exist = false
		let indexItem = null

		for (let index = 0; index < menuCart.length; index ++) {
		    if (parseInt(menuCart[index].food_id) === menu_id && menuCart[index].size === size) {
		    	indexItem = index
		     	exist = true
		     	break;
		    }
		}

		if (indexItem || indexItem == 0) {
		    $(`#addCartBtn${menu_id}${size_template[size]}`).show()
			$(`#rmvCartBtn${menu_id}${size_template[size]}`).hide()
		    menuCart.splice(indexItem, 1);
		    localStorage.setItem("menuCart", JSON.stringify(menuCart));
		}

		$('#cartTotal').text(menuCart.length)
	}
</script>