<?php require('../src/layouts/header.php'); ?>
<?php require('../controllers/utilitiesController.php');

$utility = new utilityController();
$utilityListObject = json_encode($utility->getUtilities());

$order_type = 'menu';

if (isset($_GET['type'])) {
	$order_type = $_GET['type'];
}

?>

	<div class="py-5" id="login">
		<div class="container py-xl-5 py-lg-3">
			<div class="row pt-lg-5 justify-content-md-center">
				<div class="col-sm-12 col-sm-offset-3 address-left wow agile fadeInLeft animated mt-lg-0 mt-5" data-wow-delay=".5s">
					<div class="address-grid p-sm-5 p-4">

					   <!-- Page Content -->
					  <div class="container">

					    <div class="row">

					    	<?php require('../src/navigations/steppers.php');?>

					    </div>
					    <div class="row mt-5">
					    	<div class="row">
					    	<div class="col-lg-12 mb-4">
					    		<a href="#"><h5>Cart <i class="fa fa-shopping-cart"></i> | <span id="utilityTotal" class="badge badge-primary"></span></h5></a>
					    		<div class="form-row">
					    			<div class="form-group col-md-3">
						    			<div class="input-group mt-3">
								        	<div class="input-group-prepend">
								          		<div class="input-group-text">&#8369;</div>
								        	</div>
								        	<input type="text" class="form-control" id="utilityPrice" placeholder="0.00" readonly="true">
								    	</div>
									</div>
								</div>
					    	</div>
					    	<div class="table-responsive">
				                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
				                  <thead>
				                    <tr>
				                      <th scope="col" style="width: 20%">Name</th>
				                      <th scope="col" style="width: 10%">Total</th>
				                      <th scope="col" style="width: 10%">Price</th>
				                      <th scope="col" style="width: 25%">Qty</th>
				                      <th scope="col" style="width: 20%">Option</th>
				                    </tr>
				                  </thead>
				                  <tbody id="menuTableList">
				                  <?php foreach ($utility->getUtilities() as $row) { ?>
				                    <tr>
				                    	<td><?php echo $row['utility_name']; ?></td>
				                    	<td><?php echo $row['utility_total']; ?></td>
				                    	<td><?php echo $row['utility_price']; ?></td>
				                    	<td>
				                    		<?php if ($row['utility_total'] && ($row['is_available'] == 1 || $row['is_available'] == NULL)) { ?>
							              	<div class='input-group mb-3'>
											  <div class='input-group-prepend'>
											    <button class='btn btn-outline-secondary' type='button' onclick='quantityModifier(<?php echo $row['utility_id']?>, "-")'>-</button>
											  </div>
											  <input type='text' class='utilityQuantity-control form-control' id='utilityQuantity<?php echo $row['utility_id']?>' value='0' min='0' max='100' quantity-id="<?php echo $row['utility_id']?>" aria-label='' aria-describedby='basic-addon1' readonly>
											   <div class='input-group-append'>
											    <button class='btn btn-outline-secondary' type='button' onclick='quantityModifier(<?php echo $row['utility_id']?>, "+")'>+</button>
											  </div>
											</div>
							              </div>
							              <?php } else { ?>
							              	<button type="button" class="btn btn-danger btn-sm btn-block" disabled="true">Out Of Stock</button>
							              <?php }?>
				                    	</td>
				                    	<td>
				                    		<button type="button" id="rmvBtn<?php echo $row['utility_id'] ?>" class="btn btn-danger btn-sm btn-block" onclick="removeToCart(<?php echo $row['utility_id'] ?>)" style="display: none;">Remove</button>
				                    	</td>
				                    </tr>
				                  <?php } ?>
				                  </tbody>
				                </table>
				              </div>
					        </div>
					    </div>

					    <div class="row mt5">
					    	<div class="col-lg-12 text-right">
			                  <button class="btn btn-lg btn-success" onclick="toPayment()">Next</button>
			                </div>
					    </div>

					  </div>

					</div>
				</div>
			</div>
		</div>
	</div>

	<style scope>
		.navbar {
		  /*background-color: #A1887F !important;*/
		  background-image: url("<?php echo $_ENV["base_url"]; ?>images/1.jpg") !important;
		}
	</style>

<?php require('../src/layouts/footer.php');?>

<script>
let menuCart = localStorage.getItem('menuCart') || [];
let order_type = "<?php echo $order_type; ?>";
let utility_total_price = 0;
let payment = localStorage.getItem('payment') || [];

if (order_type == 'package') {
	menuCart = localStorage.getItem('packageCart');
}

if (!menuCart.length) {
	 window.location = "<?php echo $_ENV["base_url"]; ?>views/cart.php";
}


if (typeof payment == 'string') {
	payment = JSON.parse(payment)
	console.log(payment)
	if (payment['utility_total_price'] != undefined) {
		utility_total_price = payment.utility_total_price
		$('#utilityPrice').val(`${payment.utility_total_price}`)
	}
}

let user = localStorage.getItem('user') || [];


if (!user.length) {
	window.location = "<?php echo $_ENV["base_url"]; ?>views/login.php";
}


let utilityListObject = <?php echo $utilityListObject; ?>;
let utilityCart = localStorage.getItem('utilityCart') || [];
let total = 0

if (typeof utilityCart == 'string') {
	utilityCart = JSON.parse(utilityCart)
	
	for (let index = 0; index < utilityCart.length; index++) {
		if (utilityCart[index]["utility_total_price"] != undefined ) {
			utility_total_price = parseFloat(utilityCart[index].utility_total_price) + utility_total_price
		}

		$(`#atcBtn${utilityCart[index].utility_id}`).hide();
		$(`#rmvBtn${utilityCart[index].utility_id}`).show();
		$(`#utilityQuantity${utilityCart[index].utility_id}`).val(utilityCart[index].quantity)
	}

	total = utilityCart.length;
	$('#utilityTotal').text(total);
}

function addToCart (id, quantity) {
	let utility = utilityListObject.find(function (data) {
		return data.utility_id == id
	})

	let exist = utilityCart.some(function (data) {
		return data.utility_id == id
	})

	if (!exist) {
		$(`#rmvBtn${id}`).show();
		
		if (quantity) {
			utility.quantity = quantity
			$(`#utilityQuantity${id}`).val(quantity)
		}

		utilityCart.push(utility);

		total = utilityCart.length;
		$('#utilityTotal').text(total);
		localStorage.setItem('utilityCart', JSON.stringify(utilityCart));
	}
}

$(".utilityQuantity-control").keyup(function(){
    let id = $(this).attr('quantity-id')
    let utilityQuantity = $(this).val()

    if (utilityQuantity != 0) {
      quantityModifier(id, '+', parseInt(utilityQuantity), utilityQuantity)
    } else {
      quantityModifier(id, '-', parseInt(utilityQuantity), utilityQuantity)
    }

    if (utilityQuantity == '') {
    	$(this).val(0)
    }
});


function quantityModifier (id, operator, qty = null) {
		let quantity = parseInt($(`#utilityQuantity${id}`).val());
		let utility = utilityListObject.find(function (item) {
				return parseInt(item.utility_id) === parseInt(id);
			});

		let utilityIndex = utilityCart.findIndex(function (item) {
			return parseInt(item.utility_id) === id; 
		})

		let exist = utilityCart.some(function (data) {
			return data.utility_id == id
		})

		if (operator === '+') {

			if (qty && operator != '-') {
				quantity = qty
			} else {
				quantity = quantity + 1
			}

			if (utilityIndex == -1) {
				utility_total_price = parseFloat(Math.abs(parseFloat(utility.utility_price) + utility_total_price).toFixed(2))
				payment.utility_total_price = utility_total_price;
				$('#utilityPrice').val(`${payment.utility_total_price}`)
				localStorage.setItem("payment", JSON.stringify(payment))
				this.addToCart(id, quantity, utilityIndex)
				return; 
			}

			if (exist) {
				if (quantity > parseInt(utilityCart[utilityIndex].utility_total)) {
					swal('Wait!', 'Quantity is greater than available utility', 'info');
					return;
				}
				utilityCart[utilityIndex].quantity = quantity
				utility_total_price = parseFloat(Math.abs(parseFloat(utility.utility_price) + utility_total_price).toFixed(2))
				payment.utility_total_price = utility_total_price;
				$('#utilityPrice').val(`${payment.utility_total_price}`)
				$(`#utilityQuantity${id}`).val(quantity)
				localStorage.setItem("payment", JSON.stringify(payment))
				localStorage.setItem('utilityCart', JSON.stringify(utilityCart));
			}
		}

		if (operator === '-') {
			if (exist) {
				if (quantity > 0) {
					quantity = quantity - 1
					utilityCart[utilityIndex].quantity = quantity
					utility_total_price = parseFloat(Math.abs(parseFloat(utility.utility_price) - utility_total_price).toFixed(2))
					payment.utility_total_price = utility_total_price;
					localStorage.setItem("payment", JSON.stringify(payment))
				    localStorage.setItem('utilityCart', JSON.stringify(utilityCart))
				    $('#utilityPrice').val(`${payment.utility_total_price}`)
					$(`#utilityQuantity${id}`).val(quantity)
				}

				if (quantity == 0) {
					// if (utility_total_price != 0) {
					// 	utility_total_price = parseFloat(Math.abs(parseFloat(utility.utility_price) - utility_total_price).toFixed(2))
					// 	payment.utility_total_price = utility_total_price;
					// 	localStorage.setItem("payment", JSON.stringify(payment))
					// 	$('#utilityPrice').val(`${payment.utility_total_price}`)
					// }
					this.removeToCart(id);
				}

				// if (parseFloat(utility.utility_price)  > utility_total_price) {
				// 	utility_total_price = 0;
				// 	payment.utility_total_price = utility_total_price;
				// 	localStorage.setItem("payment", JSON.stringify(payment))
				// 	$('#utilityPrice').val(`${payment.utility_total_price}`)
				// 	this.removeToCart(id);
				// }
			}
		}
}

function removeToCart (id) {
	let exist = utilityCart.some(function (data) {
		return data.utility_id == id
	})

	if (exist) {
		$(`#atcBtn${id}`).show();
		$(`#rmvBtn${id}`).hide();
		utilityCart = utilityCart.filter(utilityItem => utilityItem.utility_id != id)

		total = utilityCart.length;
		$(`#utilityQuantity${id}`).val(0)
		$('#utilityTotal').text(total);
		localStorage.setItem('utilityCart', JSON.stringify(utilityCart))
	}
}

function toPayment () {
	if (utilityCart.length) {
	window.location = `<?php echo $_ENV["base_url"]; ?>views/payment.php?type=${order_type}`;
	} else {
			swal('Wait!', 'Your Utility Cart Is Empty!', 'info');
		}
}
</script>