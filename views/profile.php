<?php require('../src/layouts/header.php');?>
<?php require('../controllers/hashController.php'); ?>
<?php
	$hash = new hashController();
	$userProfile = $_SESSION['user'];
?>

	<div class="py-5" id="login">
		<div class="container py-xl-5 py-lg-3">
			<div class="row pt-lg-5 justify-content-md-center">
				<div class="col-sm-12 col-sm-offset-3 address-left wow agile fadeInLeft animated mt-lg-0 mt-5" data-wow-delay=".5s">
					<div class="address-grid p-sm-5 p-4">

					   <!-- Page Content -->
					  <div class="container">

					    <div class="row">

					    	<div class="col-lg-12">
					    		<h1>Profile</h1>


					    	<input type="text" id="custId" value="<?php echo $userProfile["cust_id"]; ?>" hidden>
					    	<form class="mt-5">
							  <div class="form-group row">
							    <label for="staticEmail" class="col-sm-2 col-form-label">Firstname</label>
							    <div class="col-sm-10">
							      <input type="text" readonly class="form-control-plaintext" id="readFirstName" value="<?php echo $userProfile["cust_fname"]; ?>">
							      <input type="text" class="form-control" id="inputFirstName" placeholder="firstname" value="<?php echo $userProfile["cust_fname"]; ?>"style="display: none;">
							    </div>
							  </div>
							  <div class="form-group row">
							    <label for="staticEmail" class="col-sm-2 col-form-label">Middlename</label>
							    <div class="col-sm-10">
							      <input type="text" readonly class="form-control-plaintext" id="readMiddleName" value="<?php echo $userProfile["cust_mi"]; ?>">
							      <input type="text" class="form-control" id="inputMiddleName" placeholder="middlename" value="<?php echo $userProfile["cust_mi"]; ?>" style="display: none;">
							    </div>
							  </div>
							  <div class="form-group row">
							    <label for="staticEmail" class="col-sm-2 col-form-label">Lastname</label>
							    <div class="col-sm-10">
							      <input type="text" readonly class="form-control-plaintext" id="readLastName" value="<?php echo $userProfile["cust_lname"]; ?>">
							      <input type="text" class="form-control" id="inputLastName" placeholder="lastname" value="<?php echo $userProfile["cust_lname"]; ?>" style="display: none;">
							    </div>
							  </div>
							  <div class="form-group row">
							    <label for="staticEmail" class="col-sm-2 col-form-label">Email</label>
							    <div class="col-sm-10">
							      <input type="text" readonly class="form-control-plaintext" id="readEmail" value="<?php echo $userProfile["cust_email"]; ?>">
							      <input type="text" class="form-control" id="inputEmail" placeholder="email" value="<?php echo $userProfile["cust_email"]; ?>" style="display: none;">
							    </div>
							  </div>
							  <div class="form-group row">
							    <label for="staticEmail" class="col-sm-2 col-form-label">Address</label>
							    <div class="col-sm-10">
							      <input type="text" readonly class="form-control-plaintext" id="readAddress" value="<?php echo $userProfile["user_add"]; ?>">
							      <input type="text" class="form-control" id="inputAddress" placeholder="address" value="<?php echo $userProfile["user_add"]; ?>" style="display: none;">
							    </div>
							  </div>
							  <div class="form-group row">
							    <label for="staticEmail" class="col-sm-2 col-form-label">Mobile Number</label>
							    <div class="col-sm-10">
							      <input type="text" readonly class="form-control-plaintext" id="readMobileNumber" value="<?php echo $userProfile["cust_cn"]; ?>">
							      <input type="text" class="form-control" id="inputMobileNumber" placeholder="mobile number" value="<?php echo $userProfile["cust_cn"]; ?>" style="display: none;">
							    </div>
							</div>
							    <div class="form-group row">
							    <label for="staticEmail" class="col-sm-2 col-form-label">Username</label>
							    <div class="col-sm-10">
							      <input type="text" readonly class="form-control-plaintext" id="readUsername" value="<?php echo $userProfile["username"]; ?>">
							      <input type="text" class="form-control" id="inputUsername" placeholder="username" value="<?php echo $userProfile["username"]; ?>"style="display: none;">
							    </div>
							  </div>
							  <div class="form-group row">
							    <label for="inputPassword" class="col-sm-2 col-form-label">Password</label>
							    <div class="col-sm-10">
							    <input type="password" readonly class="form-control-plaintext" id="readPassword" value="<?php echo $hash->decryptHash($userProfile["password"]); ?>">
							    <input type="password" class="form-control" id="inputPassword" placeholder="**********" value="<?php echo $hash->decryptHash($userProfile["password"]); ?>" style="display: none;">
							    </div>
							  </div>
							</form>
							<button type="submit" id="editButton" class="btn btn-primary" onclick="editUser()">Edit</button>
							<button type="submit" id="cancelButton" class="btn btn-primary" onclick="cancelEdit()" style="display: none;">Cancel</button>
							<button type="submit" id="saveButton" class="btn btn-success" onclick="saveEdit()" style="display: none;">Save</button>
					    	</div>

					    </div>
					    <!-- /.row -->

					  </div>

					</div>
				</div>
			</div>
		</div>
	</div>

	<style scope>
		.navbar {
		  /*background-color: #A1887F !important;*/
		  background-image: url("<?php echo $_ENV["base_url"]; ?>/images/1.jpg") !important;
		}
	</style>


<?php require('../src/layouts/footer.php');?>


<script>
//let userInfo = <?php echo json_encode($userProfile)?>;

	function editUser(){
		$('#readFirstName').hide();
		$('#inputFirstName').show();
		$('#readMiddleName').hide();
		$('#inputMiddleName').show();
		$('#readLastName').hide();
		$('#inputLastName').show();
		$('#readEmail').hide();
		$('#inputEmail').show();
		$('#readAddress').hide();
		$('#inputAddress').show();
		$('#readMobileNumber').hide();
		$('#inputMobileNumber').show();
		$('#readUsername').hide();
		$('#inputUsername').show();
		$('#readPassword').hide();
		$('#inputPassword').show();
		$('#cancelButton').show();
		$('#editButton').hide();
		$('#saveButton').show();
	}

	function cancelEdit(){
		$('#readFirstName').show();
		$('#inputFirstName').hide();
		$('#readMiddleName').show();
		$('#inputMiddleName').hide();
		$('#readLastName').show();
		$('#inputLastName').hide();
		$('#readEmail').show();
		$('#inputEmail').hide();
		$('#readAddress').show();
		$('#inputAddress').hide();
		$('#readMobileNumber').show();
		$('#inputMobileNumber').hide();
		$('#readUsername').show();
		$('#inputUsername').hide();
		$('#readPassword').show();
		$('#inputPassword').hide();
		$('#cancelButton').hide();
		$('#editButton').show();
		$('#saveButton').hide();

	}

	function saveEdit(){
		var firstName = $('#inputFirstName').val();
		var middleName = $('#inputMiddleName').val();
		var lastName = $('#inputLastName').val();
		var email = $('#inputEmail').val();
		var address = $('#inputAddress').val();
		var mobileNumber = $('#inputMobileNumber').val();
		var username = $('#inputUsername').val();
		var password = $('#inputPassword').val();
		var custId = $('#custId').val();

	/*	if(userInfo.cust_email != email){

		}*/


	$.ajax({
      type: 'POST',
      url: '<?php echo $_ENV["base_url"]; ?>controllers/controller.php',
      data: {firstname: firstName, middlename: middleName, lastname: lastName, email: email, address: address, mobilenumber: mobileNumber, username: username, password: password, custId: custId, requestType: 'userUpdate'},
      dataType: 'JSON',
      success: function (data) {
        if (data.status != 'OK') {
          swal("Error!", data.message, "warning")
          return;
        }

        swal("Success!", data.message, "success")
        cancelEdit()

        $('#readFirstName').val(firstName);
		$('#inputFirstName').val(firstName);
		$('#readMiddleName').val(middleName);
		$('#inputMiddleName').val(middleName);
		$('#readLastName').val(lastName);
		$('#inputLastName').val(lastName);
		$('#readEmail').val(email);
		$('#inputEmail').val(email);
		$('#readAddress').val(address);
		$('#inputAddress').val(address);
		$('#readMobileNumber').val(mobileNumber);
		$('#inputMobileNumber').val(mobileNumber);
		$('#readUsername').val(username);
		$('#inputUsername').val(username);
		$('#readPassword').val(password);
		$('#inputPassword').val(password);
		
      },
      error: function (data) {
        swal("Oh no!", 'Server Error', "warning")
      }
    })

	}

</script>