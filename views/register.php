<?php require('../src/layouts/header.php');?>

	<div class="py-5" id="login">
		<div class="container py-xl-5 py-lg-3">
			<div class="row pt-lg-5 justify-content-md-center">
				<div class="col-sm-6 col-sm-offset-3 address-left wow agile fadeInLeft animated mt-lg-0 mt-5" data-wow-delay=".5s">
					<div class="address-grid p-sm-5 p-4">
						<h4 class="wow fadeIndown animated mb-3" data-wow-delay=".5s">Register</h4>
						<form>
						  <div class="form-group">
						    <label for="exampleInputEmail1">Email address</label>
						    <input type="email" class="form-control" id="email" aria-describedby="emailHelp" placeholder="Sample@gmail.com">
						  </div>
						  <div class="form-group">
						    <label for="exampleInputEmail1">First Name</label>
						    <input type="text" class="form-control" id="firstname" aria-describedby="emailHelp" placeholder="Firstname">
						  </div>
						  <div class="form-group">
						    <label for="exampleInputEmail1">Middle Initial</label>
						    <input type="text" class="form-control" id="middlename" aria-describedby="emailHelp" placeholder="Middle Initial">
						  </div>
						  <div class="form-group">
						    <label for="exampleInputEmail1">Last Name</label>
						    <input type="text" class="form-control" id="lastname" aria-describedby="emailHelp" placeholder="Lastname">
						  </div>
						  <div class="form-group">
						    <label for="exampleInputEmail1">Current Address</label>
						    <input type="text" class="form-control" id="address" aria-describedby="emailHelp" placeholder="Address">
						  </div>
						  <div class="form-group">
						    <label for="exampleInputEmail1">Mobile Number</label>
						    <input type="text" class="form-control" id="mobilenumber" aria-describedby="emailHelp" placeholder="mobile #">
						  </div>
						  <div class="form-group">
						    <label for="exampleInputEmail1">Username</label>
						    <input type="text" class="form-control" id="username" aria-describedby="emailHelp" placeholder="username">
						  </div>
						  <div class="form-group">
						    <label for="exampleInputEmail1">Password</label>
						    <input type="password" class="form-control" id="password" aria-describedby="emailHelp" placeholder="Password">
						  </div>
						  <button type="button" class="btn btn-primary" onclick="userRegister()">Submit</button>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>

	<style scope>
		.navbar {
		  /*background-color: #A1887F !important;*/
		  background-image: url("<?php echo $_ENV["base_url"]; ?>images/1.jpg") !important;
		}
	</style>


<?php require('../src/layouts/footer.php');?>

<script>
	function userRegister () {
		let regx1 = "^[a-zA-Z]+$" ;
		let username = $('#username').val()
		let firstname = $('#firstname').val()
		let middlename = $('#middlename').val()
		let lastname = $('#lastname').val()
		let email = $('#email').val()
		let address = $('#address').val()
		let mobilenumber = $('#mobilenumber').val()
		let password = $('#password').val()

		!username ? $('#username').css({'border': '1px solid red'}) : $('#username').css({'border': '1px solid green'})
		!firstname ? $('#firstname').css({'border': '1px solid red'}) : $('#firstname').css({'border': '1px solid green'})
		!middlename ? $('#middlename').css({'border': '1px solid red'}) : $('#middlename').css({'border': '1px solid green'})
		!lastname ? $('#lastname').css({'border': '1px solid red'}) : $('#lastname').css({'border': '1px solid green'})
		!email ? $('#email').css({'border': '1px solid red'}) : $('#email').css({'border': '1px solid green'})
		!address ? $('#address').css({'border': '1px solid red'}) : $('#address').css({'border': '1px solid green'})
		!mobilenumber ? $('#mobilenumber').css({'border': '1px solid red'}) : $('#mobilenumber').css({'border': '1px solid green'})
		!password ? $('#password').css({'border': '1px solid red'}) : $('#password').css({'border': '1px solid green'})

	    if (username.trim() =="" || password.trim() == "" ||!firstname.match(regx1) || !middlename.match(regx1) || !lastname.match(regx1) || email.trim() == "" || address.trim() == "" || mobilenumber.trim() == "") {
	      swal("Hey!", 'Please enter a valid input information!', "warning")
	      return;
	    }else{


	    $.ajax({
	      type: 'POST',
	      url: '<?php echo $_ENV["base_url"]; ?>controllers/controller.php',
	      data: {
	      	username: username, 
	      	password: password,
	      	firstname : firstname,
			middlename: middlename,
			lastname: lastname,
			email: email,
			address: address,
			mobilenumber: mobilenumber,
	      	requestType: 'userRegister'
	      },
	      dataType: 'JSON',
	      success: function (data) {
	        if (data.status != 'OK') {
	          swal("Error!", data.message, "warning")
	          return;
	        }

	        swal({
				title: "Congratulations!",
				text: "Account Registered!",
				type: "success",
				confirmButtonClass: "btn-success",
				confirmButtonText: "Login Now!",
			},
			function(isConfirm){
				if (isConfirm) {
					//window.location = "<?php echo $_ENV["base_url"]; ?>views/login.php";
				}
			});
	      },
	      error: function (data) {
	        swal("Oh no!", 'Server Error', "warning")
	      }
	    })
	}
}
</script>