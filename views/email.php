<?php require('../src/layouts/header.php');?>

	<div class="py-5" id="login">
		<div class="container py-xl-5 py-lg-3">
			<div class="row pt-lg-5 justify-content-md-center">
				<div class="col-sm-6 col-sm-offset-3 address-left wow agile fadeInLeft animated mt-lg-0 mt-5" data-wow-delay=".5s">
					<div class="address-grid p-sm-5 p-4">
						<h4 class="wow fadeIndown animated mb-3" data-wow-delay=".5s">Reset Password</h4>
						<form>
						  <div class="form-group">
                <label for="exampleInputPassword1">Enter Email</label>
                <input type="email" class="form-control" id="email" placeholder="Email">
              </div>
						  <button type="button" class="btn btn-primary" onclick="forgotPassword()">Submit</button>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>

	<style scope>
		.navbar {
		  /*background-color: #A1887F !important;*/
		  background-image: url("<?php echo $_ENV["base_url"]; ?>images/1.jpg") !important;
		}
	</style>


<?php require('../src/layouts/footer.php');?>
<script>
function forgotPassword () {
	let email = $('#email').val()
    $.ajax({
      type: 'POST',
      url: '<?php echo $_ENV["base_url"]?>controllers/controller.php',
      data: {email: email, requestType: 'checkEmail'},
      dataType: 'JSON',
      success: function (data) {

      	if (data.status != 'OK') {
        	adminForgotPassword(email);
          return;

        /*if (data.status != 'OK') {
          swal("Error!", data.message, "warning")
          return;*/
        }

        window.location.href = `<?php echo $_ENV["base_url"]?>views/key.php?id=${data.data.cust_id}`;        
      },
      error: function (data) {
        swal("Oh no!", 'Server Error', "warning")
      }
    })
}

function adminForgotPassword (email) {
    $.ajax({
      type: 'POST',
      url: '<?php echo $_ENV["base_url"]?>controllers/controller.php',
      data: {email: email, requestType: 'adminCheckEmail'},
      dataType: 'JSON',
      success: function (data) {

      	if (data.status != 'OK') {
        	swal("Error!", data.message, "warning")
          return;

        /*if (data.status != 'OK') {
          swal("Error!", data.message, "warning")
          return;*/
        }

        window.location.href = `<?php echo $_ENV["base_url"]?>views/key.php?id=${data.data.emp_id}`;        
      },
      error: function (data) {
        swal("Oh no!", 'Server Error', "warning")
      }
    })
}

</script>
