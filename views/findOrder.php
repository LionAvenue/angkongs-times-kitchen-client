<?php
require('../src/layouts/header.php'); 
require('../controllers/reservationController.php');

$reservation = new reservationController ;
?>
	<div class="py-5" id="login">
		<div class="container py-xl-5 py-lg-3">
			<div class="row pt-lg-5 justify-content-md-center">
				<div class="col-sm-12 col-sm-offset-3 address-left wow agile fadeInLeft animated mt-lg-0 mt-5" data-wow-delay=".5s">
					<div class="address-grid p-sm-5 p-4">

					   <!-- Page Content -->
					  <div class="container">

					    <div class="row">
					      <div class="col-lg-12">
					    		<h1 class="display-4">Track Order</h1>
					    		<form class="mt-5">
					    			<div class="input-group">
									  <div class="input-group-prepend">
									    <span class="input-group-text" id="basic-addon1">Tracking Number</span>
									  </div>
								  		<input type="text" id="trackingNumber" class="form-control" placeholder="ATK########" aria-describedby="basic-addon1">
									</div>

									<button type="button" class="btn btn-success mt-5" onclick="searchOrder()">Search Order</button>
					    		</form>
					      </div>
					      <!-- /.col-lg-9 -->

					    </div>
					    <!-- /.row -->

					  </div>

					</div>
				</div>
			</div>
		</div>
	</div>

	<style scope>
		.navbar {
		  /*background-color: #A1887F !important;*/
		  background-image: url("<?php echo $_ENV["base_url"]; ?>images/1.jpg") !important;
		}
	</style>


<?php require('../src/layouts/footer.php');?>
<script>
function searchOrder () {
	let trackingNumber = $('#trackingNumber').val()

	if (!trackingNumber) {
	swal("Hey","Please input your Tracking Number ","warning")
    return;
};

 	$.ajax({
      type: 'POST',
      url: '<?php echo $_ENV["base_url"]?>controllers/controller.php',
      data: {
        tracking_number: trackingNumber,
        requestType: 'searchOrder'
      },
      dataType: 'JSON',
      success: function (data) {
        if (data.status != 'OK') {
          swal("Oh no!", data.message, "warning")
          return;
        }

		window.location.href = `<?php echo $_ENV["base_url"]?>views/order.php?trackingNo=${trackingNumber}`;  
      },
      error: function (data) {
        swal("Oh no!", 'Server Error', "warning")
      }

    })      
}


</script>