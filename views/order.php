<?php require('../src/layouts/header.php');?>
<?php require('../controllers/reservationController.php'); ?>
<?php

$reservation = new reservationController(); 

$orderLine_id = "";

if (isset($_GET['item'])) {
	$orderLine_id = $_GET['item'];
}

if (isset($_GET['trackingNo'])) {
    $tracking_number = $_GET['trackingNo'];
    $hash = new hashController(); 
    $orderLine_id = $hash->encryptHash($reservation->getOrderIdByTracking($tracking_number));
}
?>

	<div class="py-5" id="login">
		<div class="container py-xl-5 py-lg-3">
			<div class="row pt-lg-5 justify-content-md-center">
				<div class="col-sm-12 col-sm-offset-3 address-left wow agile fadeInLeft animated mt-lg-0 mt-5" data-wow-delay=".5s">
					<div class="address-grid p-sm-5 p-4">

					   <!-- Page Content -->
					  <div class="container">

					    <div class="row">

					    	<div class="col-lg-12">
					    		<h1 class="display-4">My Order <span style="color:red" id="cancelHeader"></span></h1>
					  			<?php require('../src/navigations/tracker.php');?>
					    	</div>

					    </div>

					    <div class="row mt-5">
					    	<div class="col-lg-4">
					    		<div class="card">
								  <div class="card-body">
								    <h5 class="card-title">Shipping Address</h5>
								    <p id="address"></p>
								  </div>
								</div>

                <div class="card mt-4">
                  <div class="card-body">
                    <h5 class="card-title">Tracking Number</h5>
                    <p id="trackingNo"></p>
                  </div>
                </div>
							</div>
							<div class="col-lg-4">
					    		<div class="card">
								  <div class="card-body">
								    <h5 class="card-title">Event Date</h5>
								    <p id="eventDate"></p>
								  </div>
								</div>
							</div>
							<div class="col-lg-4">
					    		<div class="card">
								  <div class="card-body">
								    <h5 class="card-title">Payment</h5>
								    <b>Payment Method</b>
								    <p id="paymentMethod"></p>
								    <b id="headerRb" style="display: none">Remittance Branch</b>
								    <p id="remittance_provider" style="display: none"></p>
								    <b id="headerRn" style="display: none">Reference Number</b>
								    <p id="reference_number" style="display: none"></p>
								    <b id="menuTotalHeader">Menu Subtotal</b>
                    <p id="menuTotal"></p>
								    <b>Menu Total</b>
                    <p id="subTotal"></p>
                    <b>Utility Subtotal</b>
                    <p id="utilitySubTotal"></p>
                    <b id="utilitySubTotalHeader">Utility Total</b>
                    <p id="utilityPaymentTotal"></p>
                    <b>Payment Status</b>
                    <p id="paymentStatus"></p>
                    <hr>
                    <b>Total Payment</b>
                    <p id="paymentTotal"></p>
								  </div>
								</div>
							</div>
					    </div>

					    <div class="row mt-5">
							<div class="col-lg-12">
								<div class="card">
								  <div class="card-body">
								  <h5 class="card-title">Menu</h5>
									<table class="table table-bordered">
									  <thead>
									    <tr>
									      <th scope="col">Menu</th>
									      <th scope="col">Category</th>
                        <th scope="col">Size</th>
									      <th scope="col">Quantity</th>
									      <th scope="col">Price</th>
									    </tr>
									  </thead>
									  <tbody id="menuTable">
									  </tbody>
									</table>
								  </div>
								</div>
							</div>

							<div class="col-lg-12 mt-5">
								<div class="card">
								  <div class="card-body">
								  <h5 class="card-title">Utilities</h5>
									<table class="table table-bordered">
									  <thead>
									    <tr>
									      <th scope="col">Utility</th>
									      <th scope="col">Quantity</th>
                        <th scope="col">Price</th>
									    </tr>
									  </thead>
									  <tbody id="utilityTable">
									  </tbody>
									</table>
								  </div>
								</div>
							</div>

							<div class="col-lg-12 mt-5">
								<button class="btn btn-primary" id="cancelOrderBtn" onclick="cancelOrder()">Cancel Order</button>
							</div>
					    </div>
					    <!-- /.row -->

					  </div>

					</div>
				</div>
			</div>
		</div>
	</div>

	<style scope>
		.navbar {
		  /*background-color: #A1887F !important;*/
		  background-image: url("<?php echo $_ENV["base_url"]; ?>images/1.jpg") !important;
		}
	</style>


<?php require('../src/layouts/footer.php');?>
<script>
let orderLine_id = "<?php echo $orderLine_id?>";

 if (orderLine_id) {
    $.ajax({
      type: 'POST',
      url: '<?php echo $_ENV["base_url"]; ?>controllers/controller.php',
      data: {
        dataType: 'JSON',
        order_id: orderLine_id,
        requestType: 'getOrderLine'
      },
      dataType: 'JSON',
      success: function (data) {
      	console.log(data)
       	let order = data.order;
       	let orderLine = data.orderLine;
       	let services = data.service;
       	let menu = data.menu;
       	let payment = data.payment;
       	let template = '';
       	let template2 = '';
       	let full_address = `${order.address} ${order.city}`;
        let utility_total = 0;
        let utility_subtotal = 0;
        let menuTotal = 0;
        let is_package = order.package_id ? true : false;

       	if (order.cust_notif_status) {
       		updateCustomerNotification(order.order_id)
       	}

       	for (let index = 0; index < menu.length;  index++) {
          menuTotal = (parseFloat(menu[index].price_size) * parseInt(menu[index].quantity)) + menuTotal
       		template = `<tr> <td>${menu[index].food_name}</td> <td>${menu[index].category}</td> <td>${menu[index].size}</td> <td>${menu[index].quantity}</td> <td>${menu[index].price_size}</td> </tr> ` + template;
       	}

        $('#menuTotal').text(menuTotal.toFixed(2))

       	for (let index = 0; index < services.length;  index++) {
          utility_subtotal = (parseFloat(services[index].utility_price) * parseInt(services[index].quantity)) + utility_subtotal
          utility_total = parseInt(services[index].quantity) + utility_total
       		template2 = `<tr> <td>${services[index].utility_name}</td> <td>${services[index].quantity}</td> <td>${services[index].utility_price}</td> </tr> ` + template2;
       	}

        $('#utilitySubTotal').text(utility_subtotal.toFixed(2))
        $('#utilityPaymentTotal').text(utility_total)

       	if (payment['payment_type'] == 'Money Remittance') {
       		$('#headerRb').show()
		      $('#remittance_provider').show()
		      $('#headerRn').show()
		      $('#reference_number').show()
		      $('#remittance_provider').text(payment.remittance_provider)
       		$('#reference_number').text(payment.reference_number)
       	}

        if (is_package) {
          $('#menuTotal').hide()
          $('#menuTotalheader').hide()
        }

       	if(order.order_status == 'cancelled') {
       		$('#cancelOrderBtn').hide()
       		$('#cancelHeader').text('Cancelled')
       	}

       	if (order.order_status == 'preparing') {
       		$("#preparingStep").addClass("active");
       		$('#cancelOrderBtn').hide()
       	} 

       	if (order.order_status == 'delivery on process') {
       		$("#preparingStep").addClass("active");
       		$("#shippingStep").addClass("active");
       		$('#cancelOrderBtn').hide()
       	} 

       	if (order.order_status == 'delivered') {
       		$("#preparingStep").addClass("active");
       		$("#shippingStep").addClass("active");
       		$("#deliveredStep").addClass("active");
       		$('#cancelOrderBtn').hide()
       	}

       	$('#paymentMethod').text(payment.payment_type)
        $('#paymentStatus').text(payment.payment_status)
       	$('#subTotal').text(payment.subtotal)
       	$('#paymentTotal').text(order.order_payment)
        $('#trackingNo').text(order.order_tracking_no)

       	$('#menuTable').append(template)
       	$('#utilityTable').append(template2)
       	$('#address').text(full_address);
       	$('#eventDate').text(order.order_date);
      },
      error: function (data) {
        swal("Oh no!", 'Server Error', "warning")
      }
    })
  }

  function updateCustomerNotification (order_id) {
  	$.ajax({
      type: 'POST',
      url: '<?php echo $_ENV["base_url"]; ?>controllers/controller.php',
      data: {
        order_id: orderLine_id,
        requestType: 'updateCustomerNotfi'
      },
      dataType: 'JSON',
      success: function (data) {
      	 if (data.status != 'OK') {
          swal("Error!", data.message, "warning")
          return;
        }
      },
      error: function (data) {
        swal("Oh no!", 'Server Error', "warning")
      }
    })
  }

  function cancelOrder () {
  swal({
    title: "Are you sure?",
    text: "This Order will be cancelled!",
    type: "warning",
    showCancelButton: true,
    confirmButtonClass: "btn-danger",
    confirmButtonText: "Yes, cancel it!",
    closeOnConfirm: false
  },
  function(isConfirm){
    if (isConfirm) {
     $.ajax({
      type: 'POST',
      url: '<?php echo $_ENV["base_url"]; ?>controllers/controller.php',
      data: {
        order_id: orderLine_id,
        requestType: 'cancelOrder'
      },
      dataType: 'JSON',
      success: function (data) {
      	 if (data.status != 'OK') {
          swal("Error!", data.message, "warning")
          return;
        }
        $('#cancelOrderBtn').hide()
      	swal("Yeppee!", "Order Cancelled", "success")
      },
      error: function (data) {
        swal("Oh no!", 'Server Error', "warning")
      }
    })
    }
  });
  }
</script>
