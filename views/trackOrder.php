<?php require('../src/layouts/header.php');?>
<?php require('../controllers/reservationController.php'); ?>
<?php

$hash = new hashController(); 

$reservation = new reservationController(); 
$cust_id = $_SESSION['user']['cust_id'];

?>
	<div class="py-5" id="login">
		<div class="container py-xl-5 py-lg-3">
			<div class="row pt-lg-5 justify-content-md-center">
				<div class="col-sm-12 col-sm-offset-3 address-left wow agile fadeInLeft animated mt-lg-0 mt-5" data-wow-delay=".5s">
					<div class="address-grid p-sm-5 p-4">

					   <!-- Page Content -->
					  <div class="container">

					    <div class="row">

					    	<div class="col-lg-12">
					    		<h1 class="display-4">My Order</h1>

					  			<div class="table-responsive mt-5">
				                <table id="dataTable" class="display" style="width:100%">
							        <thead>
							            <tr>
							                <th>Order Status</th>
							                <th>Tracking No.</th>
							                <th>Payment</th>
							                <th>Date Ordered</th>
							                <th>Option</th>
							            </tr>
							        </thead>
							        <tbody>
							            <?php foreach ($reservation->customerReservation($cust_id) as $row) { ?>
							            	<tr>
							            		<td><?php echo $row['order_status'] ?></td>
							            		<td><?php echo $row['order_tracking_no'] ?></td>
							            		<td><?php echo $row['order_payment'] ?></td>
							            		<td><?php echo $row['created_at'] ?></td>
							            		<td class="text-center">
							            			<a href="<?php echo $_ENV["base_url"]?>views/order.php?item=<?php echo $hash->encryptHash($row['order_id']); ?>" class="btn btn-link">view</a>
							            			<br>
							            		</td>
							            	</tr>
							            <?php }?>
							        </tbody>
							    </table>
				              	</div>
					    	</div>

					    </div>
					    <!-- /.row -->

					  </div>

					</div>
				</div>
			</div>
		</div>
	</div>

	<style scope>
		.navbar {
		  /*background-color: #A1887F !important;*/
		  background-image: url("<?php echo $_ENV["base_url"]; ?>images/1.jpg") !important;
		}
	</style>

<script>
let user = localStorage.getItem('user') || [];


if (!user.length) {
	 window.location = "<?php echo $_ENV["base_url"]; ?>views/login.php";
	}
	
</script>

<?php require('../src/layouts/footer.php');?>
