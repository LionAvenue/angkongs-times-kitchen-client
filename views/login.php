<?php require('../src/layouts/header.php');?>

	<div class="py-5" id="login">
		<div class="container py-xl-5 py-lg-3">
			<div class="row pt-lg-5 justify-content-md-center">
				<div class="col-sm-6 col-sm-offset-3 address-left wow agile fadeInLeft animated mt-lg-0 mt-5" data-wow-delay=".5s">
					<div class="address-grid p-sm-5 p-4">
						<h4 class="wow fadeIndown animated mb-3" data-wow-delay=".5s">Login</h4>
						<form>
						  <div class="form-group">
						    <label for="exampleInputEmail1">Username</label>
						    <input type="email" class="form-control" id="username" aria-describedby="emailHelp" placeholder="Username">
						  </div>
						  <div class="form-group">
						    <label for="exampleInputPassword1">Password</label>
						    <input type="password" class="form-control" id="password" placeholder="Password">
						  </div>
						  <button type="button" class="btn btn-primary" onclick="userLogin()">Submit</button>
              <a href="email.php">Forgot your Password?</a>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>

	<style scope>
		.navbar {
		  /*background-color: #A1887F !important;*/
		  background-image: url("<?php echo $_ENV["base_url"]; ?>images/1.jpg") !important;
		}
	</style>


<?php require('../src/layouts/footer.php');?>
<script>
function userLogin () {
	let username = $('#username').val()
	let password = $('#password').val()

    if (!username || !password) {
      swal("Hey!", 'Please enter username and password!', "warning")
      return;
    }

    $.ajax({
      type: 'POST',
      url: '<?php echo $_ENV["base_url"]; ?>controllers/controller.php',
      data: {username: username, password: password, requestType: 'userLogin'},
      dataType: 'JSON',
      success: function (data) {
        if (data.status != 'OK') {
        	employeeLogin(username, password);
          return;
        }

        localStorage.setItem('user', JSON.stringify(data.data));
        window.location.href = "<?php echo $_ENV["base_url"]; ?>views/home.php";        
      },
      error: function (data) {
        swal("Oh no!", 'Server Error', "warning")
      }
    })
}

function employeeLogin (email, password) {
    $.ajax({
      type: 'POST',
      url: '<?php echo $_ENV["base_url"]?>controllers/controller.php',
      data: {email: email, password: password, requestType: 'employeeLogin'},
      dataType: 'JSON',
      success: function (data) {
        if (data.status != 'OK') {
          swal("Error!", data.message, "warning")
          return;
        }

        window.location.href = `<?php echo $_ENV["base_url_admin"]?>views/login.php?email=${email}&password=${password}`;        
      },
      error: function (data) {
        swal("Oh no!", 'Server Error', "warning")
      }
    })
}
</script>
