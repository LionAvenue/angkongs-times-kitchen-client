<?php require('../src/layouts/header.php');?>

	<div class="py-5" id="login">
		<div class="container py-xl-5 py-lg-3">
			<div class="row pt-lg-5 justify-content-md-center">
				<div class="col-sm-12 col-sm-offset-3 address-left wow agile fadeInLeft animated mt-lg-0 mt-5" data-wow-delay=".5s">
					<div class="address-grid p-sm-5 p-4">

					   <!-- Page Content -->
					  <div class="container">

					    <div class="row">

					    	<div class="col-lg-12">
					    		<h1>Feedback</h1>

					    		<form >
								  <div class="form-group">
								    <label for="exampleInputEmail1">Anonymous name:</label>
								    <input type="email" class="form-control" id="feedName" aria-describedby="emailHelp" placeholder="Input Any Name">
								  </div>
								  <div class="form-group">
								    <label for="exampleInputEmail1">Rating:</label>
								    <select id="feedRating">
								    	<option value="5">5</option>
								    	<option value="4">4</option>
								    	<option value="3">3</option>
								    	<option value="2">2</option>
								    	<option value="1">1</option>
								    </select>
								  </div>
								  <div class="form-group">
								    <label for="exampleFormControlTextarea1">Your Message:</label>
								    <textarea class="form-control" id="feedMsg" rows="3" placeholder="Message"></textarea>
								  </div>
								  <button type="button" class="btn btn-primary" onclick="sendFeedback()">Submit</button>
								</form>
					    	</div>

					    </div>
					    <!-- /.row -->

					  </div>

					</div>
				</div>
			</div>
		</div>
	</div>

	<style scope>
		.navbar {
		  /*background-color: #A1887F !important;*/
		  background-image: url("<?php echo $_ENV["base_url"]; ?>/images/1.jpg") !important;
		}
	</style>


<?php require('../src/layouts/footer.php');?>
<script>
	function sendFeedback () {
		let  fName = $('#feedName').val()
		let  fRate = $('#feedRating').val()
		let fMsg = $('#feedMsg').val()


		if (!fName|| !fRate|| !fMsg) {
			swal("Warning!","Please provide information in the missing field","warning")
			return;
		}
		else{

			$.ajax({
				type:'POST',
				url:'../../Angkongs-Times-Kitchen/controllers/controller.php',
				data:{'fName':fName , 'fRate':fRate, 'fMsg':fMsg , requestType:'createFeedback'},
				dataType:'JSON',
				success:function(data){
					if (data.status != "OK") {
						swal("Opps","Check backend codes!","Danger")
						return;
					}
					swal("Thank you for giving us","your honest feedback","success")
				},
				error:function(data){
					swal("Opps","Something's wrong with your code","Warning")
				}
			})
		}

		
	}
</script>