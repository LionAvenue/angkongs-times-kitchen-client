<?php require('../src/layouts/header.php');?>

	<div class="py-5" id="login">
		<div class="container py-xl-5 py-lg-3">
			<div class="row pt-lg-5 justify-content-md-center">
				<div class="col-sm-6 col-sm-offset-3 address-left wow agile fadeInLeft animated mt-lg-0 mt-5" data-wow-delay=".5s">
					<div class="address-grid p-sm-5 p-4 text-center">
						<h4 class="wow fadeIndown animated mb-3" data-wow-delay=".5s">Catering Reservation Done!</h4>
						<img src="<?php echo $_ENV["base_url"]; ?>images/chef.png" width="200">
						<a href="<?php echo $_ENV["base_url"]; ?>views/trackOrder.php" class="btn btn-primary btn-lg btn-block mt-5">Track your Order</a>
						
					</div>
				</div>
			</div>
		</div>
	</div>

	<style scope>
		.navbar {
		  /*background-color: #A1887F !important;*/
		  background-image: url("<?php echo $_ENV["base_url"]; ?>images/1.jpg") !important;
		}
	</style>


<?php require('../src/layouts/footer.php');?>
<script>
let user = localStorage.getItem('user') || [];
console.log(user);


	if (!user.length) {
	 window.location = "<?php echo $_ENV["base_url"]; ?>views/login.php";
	}


localStorage.removeItem('menuCart');
localStorage.removeItem('orderLine');
localStorage.removeItem('utilityCart');
localStorage.removeItem('payment');
</script>