<?php require('../src/layouts/header.php');?>
<?php 
require('../controllers/packageController.php');
require('../controllers/categoryController.php');
require('../controllers/hashController.php');

$hash = new hashController(); 
$package = new packageController();
$category = new categoryController();
$packageCategory = $category->getDistinctCategory('package');

if (isset($_GET['package'])) {
	$packageList = $package->getPackageList($_GET['package']);
	$packageListObject = json_encode($packageList);
} else {
	$packageList = $package->getPackageList();
	$packageListObject = json_encode($packageList);
}
?>
	<div class="py-5" id="login">
		<div class="container py-xl-5 py-lg-3">
			<div class="row pt-lg-5 justify-content-md-center">
				<div class="col-sm-12 col-sm-offset-3 address-left wow agile fadeInLeft animated mt-lg-0 mt-5" data-wow-delay=".5s">
					<div class="address-grid p-sm-5 p-4">

					   <!-- Page Content -->
					  <div class="container">

					    <div class="row">

					      <div class="col-lg-3">

					        <h1 class="my-4">Package</h1>
					        <div class="list-group">
					        <a href="<?php echo $_ENV["base_url"]; ?>views/package.php" class="list-group-item">All</a>
					        <?php foreach ($packageCategory as $row) { ?>
					        	<a href="<?php echo $_ENV["base_url"]; ?>views/package.php?package=<?php echo $row['category_name']; ?>" class="list-group-item"><?php echo $row['category_name']; ?></a>
					        <?php } ?>
					        </div>

					      </div>
					      <!-- /.col-lg-3 -->

					      <div class="col-lg-9">

					       	 <div id="carouselExampleIndicators" class="carousel slide my-4" data-ride="carousel">
					          <ol class="carousel-indicators">
					            <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
					            <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
					            <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
					          </ol>
					          <div class="carousel-inner" role="listbox">
					            <div class="carousel-item active">
					              <img class="d-block img-fluid" src="<?php echo $_ENV["base_url"]; ?>images/cover1.jpg" alt="First slide">
					            </div>
					            <div class="carousel-item">
					              <img class="d-block img-fluid" src="<?php echo $_ENV["base_url"]; ?>images/cover2.jpg" alt="Second slide">
					            </div>
					            <div class="carousel-item">
					              <img class="d-block img-fluid" src="<?php echo $_ENV["base_url"]; ?>images/cover3.png" alt="Third slide">
					            </div>
					          </div>
					          <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
					            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
					            <span class="sr-only">Previous</span>
					          </a>
					          <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
					            <span class="carousel-control-next-icon" aria-hidden="true"></span>
					            <span class="sr-only">Next</span>
					          </a>
					        </div>

					        <div class="row">
								<?php foreach ($packageList as $row) { ?>
								  <div class="col-lg-4 col-md-6 mb-4">
								    <div class="card h-100">
								      <a href="#"><img class="card-img-top" src="<?php echo $_ENV["base_url_admin"] .'/img/package_img/'. $row['package_image']?>" alt="" height="150"></a>
								      <div class="card-body">
								        <h4 class="card-title">
								          <a href="#"><?php echo $row['package_name']; ?></a>
								        </h4>
								        <h5><?php echo $row['price']; ?></h5>
								        <p class="card-text"><?php echo $row['package_desc']; ?></p>
								      </div>
								      <div class="card-footer">
								      <?php if($row['is_available'] == 0 || $row['is_available'] == null) {?>
								      <a href="<?php echo $_ENV["base_url"]?>views/packageView.php?item=<?php echo $hash->encryptHash($row['package_id']); ?>" class="btn btn-primary btn-sm btn-block">View Package</a>
								       <?php }else if ($row['is_available'] ==1) {?>
								       <button type="button" id="unavailableBtn<?php echo $row['package_id']; ?>" class="btn btn-danger btn-sm btn-block" disabled="true">Out of Stock</button>
								      <?php }?>
								      </div>
								    </div>
								  </div>
								<?php } ?>

					        </div>
					        <!-- /.row -->

					      </div>
					      <!-- /.col-lg-9 -->

					    </div>
					    <!-- /.row -->

					  </div>

					</div>
				</div>
			</div>
		</div>
	</div>

	<style scope>
		.navbar {
		  /*background-color: #A1887F !important;*/
		  background-image: url("<?php echo $_ENV["base_url"]; ?>images/1.jpg") !important;
		}
	</style>


<?php require('../src/layouts/footer.php');?>
