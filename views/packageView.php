<?php require('../src/layouts/header.php');?>
<?php require('../controllers/packageController.php'); ?>
<?php require('../controllers/hashController.php'); ?>
<?php 

$package = new packageController();

if (isset($_GET['item'])) {
  $hash = new hashController();
  $package_id = $hash->decryptHash($_GET['item']);
} else {

}

if (isset($_GET['package'])) {
  $packageList = $package->getPackageList($_GET['package']);
  $packageListObject = json_encode($packageList);
} else {
  $packageList = $package->getPackageList();
  $packageListObject = json_encode($packageList);
}
?>

?>
  <div class="py-5" id="login">
    <div class="container py-xl-5 py-lg-3">
      <div class="row pt-lg-5 justify-content-md-center">
        <div class="col-sm-12 col-sm-offset-3 address-left wow agile fadeInLeft animated mt-lg-0 mt-5" data-wow-delay=".5s">
          <div class="address-grid p-sm-5 p-4">
           
            <!-- Page Content -->
            <div class="container">

              <div class="row">

                <!-- Post Content Column -->
                <div class="col-lg-12">

                  <!-- Title -->
                  <h1 class="mt-4" id="packageName">Wedding</h1>

                  <!-- Author -->
                  <p class="lead">
                    Price
                    <a href="#" id="packagePrice">1233.3</a>
                  </p>

                  <hr>

                  <!-- Preview Image -->
                  <img class="img-fluid rounded" id="packageImage" src="http://placehold.it/900x300"  width="100%" height="300">

                  <hr>

                  <!-- Post Content -->
                  <p class="lead" id="packageDescription"></p>

                  <ul class="list-group list-group-flush mt-5" id="packageMenu">
                  </ul>

                </div>
              <?php if($has_user) { ?>
                <div class="col-lg-12">
                <form class="form-inline mt-5">
                  <div class="form-group mb-2">
                    <label for="staticEmail2" class="sr-only">Email</label>
                    <input type="text" readonly class="form-control-plaintext" id="limitHeader" value="Min 0 Max 0">
                  </div>
                  <div class="form-group mx-sm-3 mb-2">
                    <label for="inputPassword2" class="sr-only">Guess</label>
                    <input type="number" min="1" class="form-control" value="1" id="guessInput" placeholder="Guess">
                  </div>
                  <button type="button" class="btn btn-success mb-2" onclick="checkout()">Checkout</button>
                </form>
                </div>
              <?php } ?>
              </div>
              <!-- /.row -->

            </div>
            <!-- /.container -->

          </div>
        </div>
      </div>
    </div>
  </div>

  <style scope>
    .navbar {
      /*background-color: #A1887F !important;*/
      background-image: url("<?php echo $_ENV["base_url"]; ?>images/1.jpg") !important;
    }
  </style>


<?php require('../src/layouts/footer.php');?>
<script>
  let package_id = <?php echo $package_id ?>;
  let package_item = [];
  let menuCart = [];
  let orderLine = [];
  let payment = {};

  if (package_id) {
    $.ajax({
      type: 'POST',
      url: '<?php echo $_ENV["base_url"]; ?>controllers/controller.php',
      data: {
        dataType: 'JSON',
        package_id: package_id,
        requestType: 'getPackage'
      },
      dataType: 'JSON',
      success: function (data) {
        package = data.package
        package_item = data.package_item
        let template = ''
        let limit_template = `Min 1 Max ${package.limit}`

        $('#packageName').text(package.package_name)
        $('#packagePrice').text(package.price)
        $('#packageDescription').text(package.package_desc)
        $('#limitHeader').val(limit_template)
        $('#packageImage').attr('src',`<?php echo $_ENV["base_url_admin"]?>img/package_img/${package.package_image}`)

        for (let index = 0; index < package_item.length; index++) {
          menuCart.push(package_item[index])
          orderLine.push({
            "food_id": package_item[index].food_id,
            "price": package_item[index].price_size,
            "size": package_item[index].size,
            "quantity": package_item[index].quantity})

          template = `<li class='list-group-item'>${package_item[index].food_name} - ${package_item[index].size} | <span class='badge badge-primary badge-pill'>${package_item[index].quantity}</span></li>` + template
        }

        $('#packageMenu').append(template)
      },
      error: function (data) {
        swal("Oh no!", 'Server Error', "warning")
      }
    })
  }

  function checkout () {
      let total_quantity = 0;
      let package_guess = $('#guessInput').val()

      orderLine.forEach(function (order) {
        total_quantity = parseInt(order.quantity) + total_quantity;
      })

      payment.package_id = package.package_id,
      payment.total = package.price * parseInt(package_guess);
      payment.subTotal = total_quantity;
      payment.orderType = 'package';

      if (parseInt(package_guess) > parseInt(package.limit)) {
        swal("Wait!", 'Maximun Limit of Guess', "info")
      }

      localStorage.setItem('packageCart', JSON.stringify(menuCart))
      localStorage.setItem('orderLinePackage', JSON.stringify(orderLine))
      localStorage.setItem('payment', JSON.stringify(payment))
      window.location = "<?php echo $_ENV["base_url"]; ?>views/utilities.php?type=package";
  }
</script>