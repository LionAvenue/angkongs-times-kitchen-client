<?php require('../src/layouts/header.php');?>
<?php 
$order_type = 'menu';

if (isset($_GET['type'])) {
	$order_type = $_GET['type'];
}
?>
	<div class="py-5" id="login">
		<div class="container py-xl-5 py-lg-3">
			<div class="row pt-lg-5 justify-content-md-center">
				<div class="col-sm-12 col-sm-offset-3 address-left wow agile fadeInLeft animated mt-lg-0 mt-5" data-wow-delay=".5s">
					<div class="address-grid p-sm-5 p-4">

					   <!-- Page Content -->
					  <div class="container">

					    <div class="row">

					    	<?php require('../src/navigations/steppers.php');?>

					    </div>
					    <div class="row mt-5">
					    	<div class="col-lg-4">
					    		<div class="card">
								  <div class="card-body">
								    <h5 class="card-title">Payment Method</h5>
								    <p class="card-text">
								    	<select class="custom-select" id="method" onclick="paymentMethod()">
								    	  <option value="Over The Counter">Over The Counter</option>
										  <option value="Cash On Delivery">Cash On Delivery</option>
										  <option value="Money Remittance">Money Remittance</option>
										</select>
								    </p>
								     <p class="card-text mt-3">
								    	<input id="referenceNumber" type="text" class="form-control" placeholder="Reference Number" style="display: none">
								    	<input id="remittanceBranch" type="text" class="form-control mt-3" placeholder="Remittance Provider" style="display: none">
                          				<input type="file" class="form-control-file mt-3" id="referenceImage" accept="image/*" style="display: none">
								    </p>
								  </div>
								</div>


								<div class="card mt-3">
								  <div class="card-body">
								    <h5 class="card-title">Event Date</h5>
								    <p class="card-text">
								    	<input id="eventDate" type="text" class="form-control" placeholder="Event Date" data-zdp_readonly_element="false">
								    </p>
								  </div>
								</div>
								<div class="card-body">
								<div class="form-group">
			                        <label for="terms">Terms & Conditions</label><br>
			                        <form id="demo">
			                          <input type="checkbox" id="terms" name="terms[]" required> I have read and agree to the <a onclick="viewTerms()" href="javascript:void(0)">Terms & Condition</a><br>
			                        </form>
			                      </div>
			                  </div>
					    	</div>

					    	<div class="col-lg-8">
					    		<form>
								  <div class="form-row">
								    <div class="form-group col-md-6">
								      <label for="inputEmail4">Firstname</label>
								      <input type="text" class="form-control" id="firstname" placeholder="Firstname" readonly>
								    </div>
								    <div class="form-group col-md-6">
								      <label for="inputPassword4">Lastname</label>
								      <input type="text" class="form-control" id="lastname" placeholder="Lastname" readonly>
								    </div>
								  </div>
								   <div class="form-row">
								    <div class="form-group col-md-6">
								      <label for="inputEmail4">Email</label>
								      <input type="email" class="form-control" id="email" placeholder="Email" readonly>
								    </div>
								    <div class="form-group col-md-6">
								      <label for="inputPassword4">Mobile No.</label>
								      <input type="text" class="form-control" id="mobilenumber" placeholder="Mobile No." readonly>
								    </div>
								  </div>
								  <div class="form-row">
									<div class="form-group col-md-6">
									   <label for="inputAddress">Address</label>
									   <input type="text" class="form-control" id="address" placeholder="1234 Main St">
									</div>
								    <div class="form-group col-md-6">
								      <label for="inputCity">City</label>
								      <input type="text" class="form-control" id="city" placeholder="City">
								    </div>
								  </div>
								</form>
					    	</div>
					    </div>

					    <div class="row mt5">
					    	<div class="col-lg-12 text-right">
					    		<a href="<?php echo $_ENV["base_url"]; ?>views/utilities.php" class="btn btn-lg btn-primary">Previous</a>
					    		<button type="button" class="btn btn-lg btn-success" onclick="validatePayment()">
					    			Next
					    		</button>
			                </div>
					    </div>

					  </div>

					</div>
				</div>
			</div>
		</div>
	</div>


	 <!-- Modal -->
      <div class="modal fade" id="viewTermsModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title" id="exampleModalLongTitle">Terms & Condition</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
            	<p><b>TERMS AND CONDITIONS FOR CATERING SERVICES</b><br><br>
THIS FORM IS REQUIRED FOR RESERVATION BE SIGNED & RETURNED TO TIMES CUISINE or email at marilouchan96@yahoo.com<br><br>
<b><u>NUMBER OF PERSONS RESERVED:</u></b> Times Kitchen is only responsible for the reserved number of persons. Customer shall be responsible for providing extra utensils in excess for the reservations.<br><br>
<b><u>CREW’S SERVICE LIMITATIONS:</u></b> Any excess guest shall be attended to but depending on the capacity of the service crew fielded for the number of persons reserved. Preparations and cleanliness of the venue is the responsibility of the customer. Times Kitchen is not responsible for customer’s program of activities.<br><br>
<b><u>HAULING/HANDLING DELIVERIES:</u></b> Customer must see to it that the given sketch and/or location map is precise and accurate to avoid delay. Times Kitchen is not responsible for the delayed hauling due to uncontrollable incidents liked closed roads, detours, public disturbances, non-made and natural calamities.<br><br>
<b><u>LEFTOVERS:</u></b> Times Kitchen is no longer responsible for food leftover after three (3) hours from the commencement of catering services. Customer is responsible for providing own food storage container or plastic bags for leftover foods.<br><br>
<b><u>BREAKAGE/LOSS:</u></b> Customer shall be responsible for loss or damage of utensils or equipment.<br><br>
<b><u>CANCELLATIONS AND REFUND:</u></b> Cancellation of engagement for at least (2) two days before the agreed date shall entitled the customer to returned of down payment subject of deduction for whatever expenses incurred in the preparation for the said engagement. Notice	 of cancellation must be done in writing.<br><br>
<b><u>FORFEITURE:</u></b> Cancellation on the day of catering service shall result in the forfeiture of payments made to cover expenses for preparations for the agreed service. Customer can claim 50% of down payment if cancellation notice is duly served one (1) day and prior days before the reserved date.<br><br>
<b><u>MENU CHANGES:</u></b> Customer can make changes of menus two days before the agreed service date.<br><br>
<b><u>CHARGES:</u></b> Entrance fees in resort, subdivisions, villages and additional transportations cost for hilly and mountainous location of the service shall be at the account of the customer. Services beyond Talisay City and Mandaue City shall be subject to separate charges.<br><br>
<b><u>DOWN PAYMENT AND FULL PAYMENT:</u></b> Upon signing of the agreement, customer shall pay a down payment of 50 percent and the remaining balance shall be made on the day of the catering service.<br><br>
<b><u>FORM OF PAYMENT OF SERVICES:</u></b> Customer shall pay all the remaining balance of the original contract price, including the charges for additional services, on the day of service and it shall be paid cash.<br><br>
	HEREIN PARTIES HEREBY express their conformity to the foregoing and manifest that we have fully understood and executed the same voluntarily and freely.<br><br>
</p>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
          </div>
        </div>
      </div>



	<style scope>
		.navbar {
		  /*background-color: #A1887F !important;*/
		  background-image: url("<?php echo $_ENV["base_url"]; ?>images/1.jpg") !important;
		}
	</style>

<?php require('../src/layouts/footer.php');?>
<script>
	let user = localStorage.getItem('user') || [];
	let payment = localStorage.getItem('payment') || {};
	let url_order_type = "<?php echo $order_type; ?>";

	let orderType = "";

	if (!user.length) {
	 window.location = "<?php echo $_ENV["base_url"]; ?>views/login.php";
	}


	 $('#eventDate').Zebra_DatePicker({
	 	 format: 'Y-m-d h:i:A',
        direction: true
    });

	if (typeof user == 'string') {
		user = JSON.parse(user);

		$('#firstname').val(user.cust_fname);
		$('#lastname').val(user.cust_lname);
		$('#email').val(user.cust_email);
		$('#mobilenumber').val(user.cust_cn);
	} else {
		window.location = "<?php echo $_ENV["base_url"]; ?>views/login.php";
	}

	if (typeof payment == 'string') {
		payment = JSON.parse(payment);

		$('#address').val(payment.address);
		$('#method').val(payment.method);
		$('#city').val(payment.city);
		$('#eventDate').val(payment.eventDate);

		if (payment.method == 'Money Remittance') {
			$('#referenceNumber').show()
			$('#remittanceBranch').show()
			$('#referenceImage').show()
			$('#referenceNumber').val(payment.referenceNumber)
			$('#remittanceBranch').val(payment.remittanceBranch)
		}
	}

	function paymentMethod () {
		let method = $('#method').val();
		let time_start = moment("08:00:00 am", "hh:mm:ss a")
		let time_end = moment("10:00:00 pm", "hh:mm:ss a")
		let time_today = moment().format("hh:mm:ss a")

		payment.method = method;

		if (method == 'Money Remittance') {

			if (time_start.isSameOrAfter(moment(time_today, 'hh:mm:ss a')) ||  time_end.isSameOrBefore(moment(time_today, 'hh:mm:ss a'))) {
				swal('OPPS!', 'Money Remittance is unavailable at this time', 'warning')
				$('#method').val('Cash On Delivery');
				return;
			}

			$('#referenceNumber').show()
			$('#remittanceBranch').show()
			$('#referenceImage').show()
		} else {
			$('#referenceNumber').hide()
			$('#referenceNumber').val('')
			$('#remittanceBranch').val('')
			$('#remittanceBranch').hide()
			$('#referenceImage').hide()
		}

		localStorage.setItem('payment', JSON.stringify(payment));
	}

	function validatePayment () {
		let method = $('#method').val();
		let address = $('#address').val();
		let city = $('#city').val();
		let eventDate = $('#eventDate').val();
		let referenceNumber = $('#referenceNumber').val();
		let remittanceBranch = $('#remittanceBranch').val();
		let file_data = $('#referenceImage').prop('files')[0]
		let time_today = moment().format("Y-MM-DD hh:mm:ss a")

		if (method === 'Money Remittance') {
			if (!file_data) {
				swal('Wait!', 'Please upload you reference image.', 'info')
				return;
			}

			payment.referenceImage = file_data.name
			uploadReferenceImage (file_data)
		}

		if (moment(eventDate).isBefore(time_today))
		{
			swal('Wait!', 'Invalid order before time and date', 'info')
			return;
		}

		if (!$('#terms').is(':checked')) {
			swal('Wait!', 'Please Agree to our Terms & Condition.', 'info')
			return;
		}



		!method ? $('#method').css({'border': '1px solid red'}) : $('#method').css({'border': '1px solid green'})
		!eventDate ? $('#eventDate').css({'border': '1px solid red'}) : $('#eventDate').css({'border': '1px solid green'})
		!address ? $('#address').css({'border': '1px solid red'}) : $('#address').css({'border': '1px solid green'})
		!city ? $('#city').css({'border': '1px solid red'}) : $('#city').css({'border': '1px solid green'})


		if (!method || !address || !city || !eventDate) {
			swal('Wait!', 'Please Input your Payment Information!', 'info');
			return;
		}

		if (referenceNumber) {
			payment.referenceNumber = referenceNumber;
			payment.remittanceBranch = remittanceBranch;
		}


		payment.method = method;
		payment.city = city;
		payment.address = address;
		payment.cust_id = user.cust_id;
		payment.eventDate = eventDate;
		localStorage.setItem('payment', JSON.stringify(payment));
		window.location = `<?php echo $_ENV["base_url"]; ?>views/checkout.php?type=${url_order_type}`;
	}

	function uploadReferenceImage (file_data) {
		let form_data = new FormData()                  
	    form_data.append('packageFile', file_data)
	    form_data.append('requestType', 'uploadReferenceImage')

	    $.ajax({
	      type: 'POST',
	      url: '<?php echo $_ENV["base_url"]?>controllers/controller.php',
	      contentType: false,
	      cache: false,
	      processData:false,
	      data: form_data,
	      dataType: 'JSON',
	      success: function (data) {
	        if (data.status != 'OK') {
	          swal("Oh no!", data.message, "warning")
	          return;
	        }
		
			console.log(data)      
	      },
	      error: function (data) {
	        console.log(data)
	      }
	    })
	}

	function viewTerms() {

	$('#viewTermsModal').modal('show')

	}
</script>