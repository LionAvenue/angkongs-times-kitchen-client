<?php require('../src/layouts/header.php');?>

	<div class="py-5" id="login">
		<div class="container py-xl-5 py-lg-3">
			<div class="row pt-lg-5 justify-content-md-center">
				<div class="col-sm-12 col-sm-offset-3 address-left wow agile fadeInLeft animated mt-lg-0 mt-5" data-wow-delay=".5s">
					<div class="address-grid p-sm-5 p-4">

					   <!-- Page Content -->
					  <div class="container">

					    <div class="row">

					    	<div class="col-lg-12">
					    		<h1 style="text-align: center;">About Us</h1>
					    		<br>
					    		<div style="float: left;">
					    		<p style="font: italic bold 12px/30px Georgia, serif; text-align: center; margin-left: 50px;">Juliette hasn’t touched anyone in exactly 264 days.<br>The last time she did, it was an accident,<br> but The Reestablishment locked her up for murder. </p>
						</div>

						<div style="float: right;">
					    		<p style="font: italic bold 12px/30px Georgia, serif; text-align: center; margin-right: 50px;">Juliette hasn’t touched anyone in exactly 264 days.<br>The last time she did, it was an accident,<br> but The Reestablishment locked her up for murder. </p>
					    	</div>

					    	<div>
					    		<img src="../images/cover1.jpg" style="width: 30%; height: 30%; margin-top: 20px; margin-left:50px;">
					    		<img src="../images/cover2.jpg" style="width: 30%; height: 30%; margin-top: 20px;">
					    		<img src="../images/cover3.png" style="width: 30%; height: 30%; margin-top: 20px;">
					    	</div>

							<div style="float: left; margin-top: 30px; background-color: #D4D4D4; width: 50%;">
					    		<h3 style="font: italic bold 12px/30px Georgia, serif; text-align: left; margin: 50px;">Juliette hasn’t touched anyone in exactly 264 days.<br>The last time she did, it was an accident,<br> but The Reestablishment locked her up for murder.The world is too busy crumbling to pieces to pay attention to a 17-year-old girl. Diseases are destroying the population, food is hard to find, birds don’t fly anymore, and the clouds are the wrong color.</h3>
							</div>

							<div>
								<img src="../images/phone.jpg" style="width: 5%; height: 5%; margin-top: 100px; margin-left: 100px;">
							</div>
							<div>
								<img src="../images/email.jpg" style="width: 5%; height: 5%; margin-top: 30px; margin-left: 100px;">
							</div>
							<div>
								<img src="../images/location.jpg" style="width: 5%; height: 5%; margin-top: 30px; margin-left: 100px;">
							</div>
							<br><br><br><br><br><br>
							<div class="map-responsive" style="margin-left: 50px">
								<center><h1>Visit Us</h1></center>
								<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3925.326110078471!2d123.88774921479678!3d10.315765092635138!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x33a9994a38131213%3A0xcbff4728bd0b617!2sTimes+Kitchen!5e0!3m2!1sen!2sph!4v1554737659001!5m2!1sen!2sph" width="900" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
							</div>
						
					    		
					    	</div>
					    	

					    </div>
					    <!-- /.row -->

					  </div>

					</div>
				</div>
			</div>
		</div>
	</div>

	<style scope>
		.navbar {
		  /*background-color: #A1887F !important;*/
		  background-image: url("<?php echo $_ENV["base_url"]; ?>images/1.jpg") !important;
		}
	</style>


<?php require('../src/layouts/footer.php');?>
