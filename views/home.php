	<!-- header -->
	<?php require('../src/layouts/header.php');?>
	<?php require('../src/navigations/carousel.php');?>

	<!-- about -->

	<div class="about" id="about" >
		<div class="container py-xl-5 py-lg-3">
			<div class="row pt-lg-5 justify-content-md-center">
				<div class="col-sm-12 col-sm-offset-3 address-left wow agile fadeInLeft animated mt-lg-0 mt-5" data-wow-delay=".5s">
					<div class="address-grid p-sm-5 p-4">

					   <!-- Page Content -->
					  <div class="container">

					    <div class="row">

					    	<div class="col-lg-12">
					    		<h1 style="text-align: center;">About Us</h1>
					    		<br>
					    		<div style="float: left;">
					    		<p style="font: italic bold 12px/30px Georgia, serif; text-align: center; margin-left: 50px;">Juliette hasn’t touched anyone in exactly 264 days.<br>The last time she did, it was an accident,<br> but The Reestablishment locked her up for murder. </p>
						</div>

						<div style="float: right;">
					    		<p style="font: italic bold 12px/30px Georgia, serif; text-align: center; margin-right: 50px;">Juliette hasn’t touched anyone in exactly 264 days.<br>The last time she did, it was an accident,<br> but The Reestablishment locked her up for murder. </p>
					    	</div>

					    	<div>
					    		<img src="../images/cover1.jpg" style="width: 30%; height: 30%; margin-top: 20px; margin-left:50px;">
					    		<img src="../images/cover2.jpg" style="width: 30%; height: 30%; margin-top: 20px;">
					    		<img src="../images/cover3.png" style="width: 30%; height: 30%; margin-top: 20px;">
					    	</div>

							<div style="float: left; margin-top: 30px; background-color: #D4D4D4; width: 50%;">
					    		<h3 style="font: italic bold 12px/30px Georgia, serif; text-align: left; margin: 50px;">Juliette hasn’t touched anyone in exactly 264 days.<br>The last time she did, it was an accident,<br> but The Reestablishment locked her up for murder.The world is too busy crumbling to pieces to pay attention to a 17-year-old girl. Diseases are destroying the population, food is hard to find, birds don’t fly anymore, and the clouds are the wrong color.</h3>
								
							</div>

							<div>
								<img src="../images/phone.jpg" style="width: 5%; height: 5%; margin-top: 100px; margin-left: 100px;">
							</div>
							<div>
								<img src="../images/email.jpg" style="width: 5%; height: 5%; margin-top: 30px; margin-left: 100px;">
							</div>
							<div>
								<img src="../images/location.jpg" style="width: 5%; height: 5%; margin-top: 30px; margin-left: 100px;">
							</div>
							<br><br><br><br><br><br>
							<div class="map-responsive" style="margin-left: 50px">
								<center><h1>Visit Us</h1></center>
								<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3925.326110078471!2d123.88774921479678!3d10.315765092635138!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x33a9994a38131213%3A0xcbff4728bd0b617!2sTimes+Kitchen!5e0!3m2!1sen!2sph!4v1554737659001!5m2!1sen!2sph" width="900" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
							</div>
						
							
						
					    		
					    	</div>
					    	

					    </div>
					    <!-- /.row -->

					  </div>

					</div>
				</div>
			</div>
		</div>
	</div>


<!--

	 <div class="about py-5" id="about">
		<div class="container py-xl-5 py-lg-3">
			<h3 class="title text-center text-dark mb-sm-5 mb-4">
				<span>Welcome to our Snacks</span> world's no.1 tasty food</h3>
			<div class="row about-bottom-w3l text-center pt-lg-5">
				<div class="col-sm-6 about-grid">
					<div class="about-grid-main">
						<img src="<?php echo $_ENV["base_url"]; ?>images/a1.jpg" alt="" class="img-fluid">
						<h4 class="mb-3">Clean & Continental Place</h4>
						<p> Ut enim ad minim veniam, quis nostrud exercitation ullamco</p>
						<a href="#" class="button-w3ls mt-4" data-toggle="modal" data-target="#exampleModalCenter1">Read More
							<i class="fas fa-long-arrow-alt-right ml-3"></i>
						</a>
					</div>
				</div>
				<div class="col-sm-6 about-grid mt-sm-0 mt-4">
					<div class="about-grid-main">
						<img src="<?php echo $_ENV["base_url"]; ?>images/a2.jpg" alt="" class="img-fluid">
						<h4 class="mb-3">100% Healthy Food</h4>
						<p>Ut enim ad minim veniam, quis nostrud exercitation ullamco</p>
						<a href="#" class="button-w3ls mt-4" data-toggle="modal" data-target="#exampleModalCenter1">Read More
							<i class="fas fa-long-arrow-alt-right ml-3"></i>
						</a>
					</div>
				</div>
			</div>
		</div>
	</div>  -->
	<!-- //about -->

	<!-- services -->
	<!--<div class="w3ls-middle pt-5" id="services">
		<div class="container py-xl-5 py-lg-3">
			<h3 class="title text-center text-dark mb-sm-5 mb-4">
				<span>The great advantages</span> of a hotel is that it is a refuge from home life</h3>
			<div class="row pt-lg-5">
				<div class="col-lg-6 text-center">
					<img src="<?php echo $_ENV["base_url"]; ?>images/3.png" alt="" class="img-fluid chef-img">
				</div>
				<div class="col-lg-4 mt-lg-0 mt-4">
					<div class="service1-wthree">
						<h4>1</h4>
						<h6 class="text-dark mt-3 mb-2">Meal Delivery</h6>
						<p>sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
					</div>
					<div class="service1-wthree my-4">
						<h4>2</h4>
						<h6 class="text-dark mt-4 mb-2">Catering</h6>
						<p>sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
					</div>
					<div class="service1-wthree">
						<h4>3</h4>
						<h6 class="text-dark mt-4 mb-2">Events</h6>
						<p>sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
					</div>
				</div>
				<div class="offset-lg-2"></div>
			</div>
		</div>
	</div>-->
	<!-- //services -->

	<!-- middle section one -->
	<!--<div class="w3ls-middle-works py-5 text-center" id="specials">
		<div class="py-xl-5 py-lg-3">
			<h3 class="title text-center text-dark mb-lg-5">
				<span>Fresh, organic ingredients,</span>Carefully prepared, Eat green for a reason.</h3>
			<a href="#" class="button-w3ls my-4 text-center" data-toggle="modal" data-target="#exampleModalCenter1">Read more about Specials
				<i class="fas fa-long-arrow-alt-right ml-3"></i>
			</a>
			<div class="d-flex special-agiles text-left">
				<div class="col-md-6 grids-w3ls-left">
					<p class="mb-2 text-white">Our Specials</p>
					<h4 class="title text-dark mb-md-4">Carefully prepared, Fresh, organic ingredients.</h4>
					<a href="#" class="button-w3ls mt-4" data-toggle="modal" data-target="#exampleModalCenter1">Read More
						<i class="fas fa-long-arrow-alt-right ml-3"></i>
					</a>
				</div>
				<div class="col-md-6 grids-w3ls-right">
					<img src="<?php echo $_ENV["base_url"]; ?>images/middle.jpg" alt="" class="img-fluid">
				</div>
			</div>
		</div>
	</div>-->
	<!-- //middle section one -->

	<!-- middle section two -->
	<!--<div class="agile-wthree-works py-5" id="some">
		<div class="container py-xl-5 py-lg-3">
			<div class="row">
				<div class="grids-w3ls-right-2 offset-lg-7 offset-sm-4">
					<h4 class="title text-dark mb-sm-5 mb-4">Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut
						fugit</h4>
					<p class="mt-4">sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad reprehenderit qui in
						ea voluptate velit
						esse </p>
					<a href="#" class="button-w3ls mt-5" data-toggle="modal" data-target="#exampleModalCenter1">Read More
						<i class="fas fa-long-arrow-alt-right ml-3"></i>
					</a>
				</div>
			</div>
		</div>
	</div>-->

	<!-- contact -->
	<!-- contact -->
	<div class="feedback" id="feedback" style="background: url(../images/bg3.jpg) no-repeat center;">
		<div class="container py-xl-5 py-lg-3">
			<h3 class="title text-center text-dark mb-sm-5 mb-4">
				<span>Feedback</span> Give us your feedback/suggestions</h3>
			<div id="feedbackTemplate" class="row address-row pt-lg-5 text-center">	
			</div>
		</div>
	</div>
	<!--//contact-->
	<?php require('../src/layouts/footer.php');?>
	<script>
		$( document ).ready(function() {
    		$.ajax({
		      type: 'POST',
		      url: '<?php echo $_ENV["base_url"]; ?>controllers/controller.php',
		      data: {requestType: 'getFeedback'},
		      dataType: 'JSON',
		      success: function (data) {
		        if (data.status != 'OK') {
		          swal("Error!", data.message, "warning")
		          return;
		        }

		        let template = '';
			
				for (let index = 0; index < data.data.length; index++) {
					template = `<div class='col-lg-4'>
						<div class='address-right p-sm-5 p-4'>
							<div class='address-info wow fadeInRight animated' data-wow-delay='.5s'>
								<h4 class='mb-3'>${data.data[index].user_name}</h4>
								<p>${data.data[index].feedback_message}</p>
							</div>
						</div>
					</div>` + template;    	
				}

				$('#feedbackTemplate').append(template)
		      },
		      error: function (data) {
		        swal("Oh no!", 'Server Error', "warning")
		      }
		    })
		})
	</script>