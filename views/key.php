<?php require('../src/layouts/header.php');?>
<?php
$user_id = null; 
if (isset($_GET['id'])) {
  $user_id = $_GET['id'];
}

?>
	<div class="py-5" id="login">
		<div class="container py-xl-5 py-lg-3">
			<div class="row pt-lg-5 justify-content-md-center">
				<div class="col-sm-6 col-sm-offset-3 address-left wow agile fadeInLeft animated mt-lg-0 mt-5" data-wow-delay=".5s">
					<div class="address-grid p-sm-5 p-4">
						<h4 class="wow fadeIndown animated mb-3" data-wow-delay=".5s">Enter Temporary Key</h4>
						<form>
						  <div class="form-group">
                <label for="exampleInputPassword1">Key</label>
                <input type="text" class="form-control" id="key" placeholder="key">
              </div>
						  <button type="button" class="btn btn-primary" onclick="forgotPassword()">Submit</button>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>

	<style scope>
		.navbar {
		  /*background-color: #A1887F !important;*/
		  background-image: url("<?php echo $_ENV["base_url"]; ?>images/1.jpg") !important;
		}
	</style>


<?php require('../src/layouts/footer.php');?>
<script>
let user_id = <?php echo $user_id?>;
if (!user_id) {
      window.location.href = `<?php echo $_ENV["base_url"]?>views/email.php`;
}

function forgotPassword () {
  let key = $('#key').val()
  let user_id = <?php echo $user_id?>;
    $.ajax({
      type: 'POST',
      url: '<?php echo $_ENV["base_url"]?>controllers/controller.php',
      data: {key: key, requestType: 'checkKey'},
      dataType: 'JSON',
      success: function (data) {
        if (data.status != 'OK') {
          adminForgotPassword();
          return;
        }

        window.location.href = `<?php echo $_ENV["base_url"]?>views/forgot.php?id=${user_id}`;      
      },
      error: function (data) {
        swal("Oh no!", 'Server Error', "warning")
      }
    })
}

function adminForgotPassword () {
  let key = $('#key').val()
  let user_id = <?php echo $user_id?>;
    $.ajax({
      type: 'POST',
      url: '<?php echo $_ENV["base_url"]?>controllers/controller.php',
      data: {key: key, requestType: 'adminCheckKey'},
      dataType: 'JSON',
      success: function (data) {
        if (data.status != 'OK') {
          swal("Error!", data.message, "warning")
          return;
        }

        window.location.href = `<?php echo $_ENV["base_url"]?>views/adminForgot.php?id=${user_id}`;      
      },
      error: function (data) {
        swal("Oh no!", 'Server Error', "warning")
      }
    })
}
</script>
