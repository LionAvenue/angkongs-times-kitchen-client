<?php require('../src/layouts/header.php');?>
<?php 
$order_type = 'menu';

if (isset($_GET['type'])) {
	$order_type = $_GET['type'];
}
?>
	<div class="py-5" id="login">
		<div class="container py-xl-5 py-lg-3">
			<div class="row pt-lg-5 justify-content-md-center">
				<div class="col-sm-12 col-sm-offset-3 address-left wow agile fadeInLeft animated mt-lg-0 mt-5" data-wow-delay=".5s">
					<div class="address-grid p-sm-5 p-4">

					   <!-- Page Content -->
					  <div class="container">

					    <div class="row">

					    	<?php require('../src/navigations/steppers.php');?>

					    </div>
					    <div class="row mt-5 mb-5">
					    	<div class="col-lg-8">
					    	<h3>Menu Items:</h3>
				    		<table class="table table-striped mt-3">
							  <thead>
							    <tr>
							      <th scope="col">Menu</th>
							      <th scope="col">Category</th>
							      <th scope="col">Size</th>
							      <th scope="col">Quantity</th>
							      <th scope="col">Price</th>
							    </tr>
							  </thead>
							  <tbody id="menuTable">
							  </tbody>
							</table>

							<h3 class="mt-5">Utility Items:</h3>
				    		<table class="table table-striped mt-3">
							  <thead>
							    <tr>
							      <th scope="col">Utility</th>
							      <th scope="col">Quantity</th>
							      <th scope="col">Price</th>
							    </tr>
							  </thead>
							  <tbody id="utilityTable">
							  </tbody>
							</table>
					    	</div>

					    	<div class="col-lg-4">
					    		<div class="card">
								  <div class="card-body">
								    <h5 class="card-title">Payment Summary</h5>
								    <hr>
								    <p class="card-text">
										<form>
										  <div class="form-group">
										    <label for="exampleInputEmail1">Payment Method:</label>
										     <input type="text" readonly class="form-control-plaintext" id="paymentMethod" value="payment">
										  </div>
										  <div class="form-group" style="display: none" id="remittanceBranchForm">
										    <label for="exampleInputEmail1">Remittance Branch:</label>
										     <input type="text" readonly class="form-control-plaintext" id="remittanceBranch" value="Remittance Branch">
										  </div>
										  <div class="form-group" style="display: none" id="referenceNumberForm">
										    <label for="exampleInputEmail1">Reference Number:</label>
										     <input type="text" readonly class="form-control-plaintext" id="referenceNumber" value="referenceNumber">
										  </div>
										  <div class="form-group">
										    <label for="exampleInputEmail1">Order Type:</label>
										     <input type="text" readonly class="form-control-plaintext" id="orderType" value="orderType">
										  </div>
										  <div class="form-group">
										    <label for="exampleInputEmail1">Event Date:</label>
										     <input type="text" readonly class="form-control-plaintext" id="eventDate" value="event date">
										  </div>
										  <div class="form-group">
										    <label for="exampleInputEmail1">No. of Menu items:</label>
										     <input type="text" readonly class="form-control-plaintext" id="subTotal" value="subTotal">
										  </div>
										  <div class="form-group">
										    <label for="exampleInputEmail1">Menu Subtotal:</label>
										     <input type="text" readonly class="form-control-plaintext" id="menuTotal" value="price">
										  </div>
										  <div class="form-group">
										    <label for="exampleInputEmail1">No. of Utility items:</label>
										     <input type="text" readonly class="form-control-plaintext" id="utilitySubTotal" value="0.00">
										  </div>
										  <div class="form-group">
										    <label for="exampleInputEmail1">Utility Subtotal:</label>
										     <input type="text" readonly class="form-control-plaintext" id="utilityTotal" value="price">
										  </div>
										  <div class="form-group">
										  <label for="exampleInputEmail1">Total Price:</label>
										  	<div class="input-group mt-3">
								        		<div class="input-group-prepend">
								          			<div class="input-group-text">&#8369;</div>
								        		</div>
								        		<input type="text" class="form-control" id="finalPrice" placeholder="0.00" readonly="true">
								    		</div>
										  </div>
										</form>
								    </p>
								  </div>
								</div>

								<div class="card mt-3">
								  <div class="card-body">
								    <h5 class="card-title">Customer Information</h5>
								    <hr>
								    <p class="card-text">
										<form>
										  <div class="form-group">
										    <label for="exampleInputEmail1">Firstname:</label>
										     <input type="text" readonly class="form-control-plaintext" id="firstname">
										  </div>
										  <div class="form-group">
										    <label for="exampleInputEmail1">Lastname:</label>
										     <input type="text" readonly class="form-control-plaintext" id="lastname">
										  </div>
										  <div class="form-group">
										    <label for="exampleInputEmail1">Email:</label>
										     <input type="text" readonly class="form-control-plaintext" id="email">
										  </div>
										  <div class="form-group">
										    <label for="exampleInputEmail1">Mobile Number:</label>
										     <input type="text" readonly class="form-control-plaintext" id="mobilenumber">
										  </div>
										  <div class="form-group">
										    <label for="exampleInputEmail1">Address:</label>
										     <input type="text" readonly class="form-control-plaintext" id="address">
										  </div>
										  <div class="form-group">
										    <label for="exampleInputEmail1">City:</label>
										     <input type="text" readonly class="form-control-plaintext" id="city">
										  </div>
										</form>
								    </p>
								  </div>
								</div>
					    	</div>
					    </div>

					    <div class="row mt5">
					    	<div class="col-lg-12 text-right">
					    		<a href="<?php echo $_ENV["base_url"]; ?>views/payment.php" class="btn btn-lg btn-primary">Previous</a>
			                 	<button type="button" class="btn btn-lg btn-success" onclick="userReservation()">
			                 		Done
			                 	</button>
			                </div>
					    </div>

					  </div>

					</div>
				</div>
			</div>
		</div>
	</div>

	<style scope>
		.navbar {
		  /*background-color: #A1887F !important;*/
		  background-image: url("<?php echo $_ENV["base_url"]; ?>images/1.jpg") !important;
		}
	</style>


<?php require('../src/layouts/footer.php');?>
<script>
	let menuCart = localStorage.getItem('menuCart') || [];
	let orderLine = localStorage.getItem('orderLine') || [];
    let utilityCart = localStorage.getItem('utilityCart') || [];
    let user = localStorage.getItem('user') || [];
    let payment = localStorage.getItem('payment') || [];
    let url_order_type = "<?php echo $order_type; ?>";
    let finalPrice = 0.00
    let total_utility = 0

    if (url_order_type == 'package') {
    	menuCart = localStorage.getItem('packageCart');
    	orderLine = localStorage.getItem('orderLinePackage')
    }

    if (!menuCart.length || !orderLine) {
		window.location = "<?php echo $_ENV["base_url"]; ?>views/menu.php" ;
	}

	if (!utilityCart.length) {
		window.location = `<?php echo $_ENV["base_url"]; ?>views/utilities.php?type=${url_order_type}`;
	}

	if (!user.length) {
	 window.location = "<?php echo $_ENV["base_url"]; ?>views/login.php";
	}

	
	if (typeof menuCart == 'string') {
		let template = ''
		menuCart = JSON.parse(menuCart)

		for (let index = 0; index < menuCart.length; index++) {
			template = `<tr> <td>${menuCart[index].food_name}</td> <td>${menuCart[index].category}</td> <td>${menuCart[index].size}</td> <td id='menuRowQty${menuCart[index].food_id}'>1</td> <td>${menuCart[index].price_size}</td> </tr>` + template
		}

		$('#menuTable').append(template)
	}


	if (typeof orderLine == 'string') {
		let template = ''
		orderLine = JSON.parse(orderLine)

		for (let index = 0; index < orderLine.length; index++) {
			$(`#menuRowQty${orderLine[index].food_id}`).text(orderLine[index].quantity)
		}
	}

	if (typeof utilityCart == 'string') {
		let template = ''
		utilityCart = JSON.parse(utilityCart)

		for (let index = 0; index < utilityCart.length; index++) {
			total_utility = utilityCart[index].quantity + total_utility
			template = `<tr> <td>${utilityCart[index].utility_name}</td> <td>${utilityCart[index].quantity}</td> <td>${utilityCart[index].utility_price}</td></tr>` + template
		}

		$('#utilitySubTotal').val(total_utility)
		$('#utilityTable').append(template)
	}

	if (typeof payment == 'string') {
		payment = JSON.parse(payment);

		if (payment['eventDate'] == undefined || 
			payment['method'] == undefined || 
			payment['address'] == undefined ||
			payment['city'] == undefined ) {
			window.location = `<?php echo $_ENV["base_url"]; ?>views/payment.php?type=${url_order_type}`;
		}


		if (payment['method'] == "Money Remittance") {
			if (payment['referenceNumber'] == undefined || payment['remittanceBranch'] == undefined) {
				window.location = `<?php echo $_ENV["base_url"]; ?>views/payment.php?type=${url_order_type}`;
			}
		}

		finalPrice = parseFloat(parseFloat(payment.total) + payment.utility_total_price)

		$('#address').val(payment.address)
		$('#menuTotal').val(payment.total)
		$('#utilityTotal').val(payment.utility_total_price)
		$('#finalPrice').val(finalPrice.toFixed(2))
		$('#city').val(payment.city)
		$('#paymentMethod').val(payment.method)
		$('#eventDate').val(payment.eventDate)
		$('#totalPayment').val(payment.total)
		$('#subTotal').val(payment.subTotal)
		$('#orderType').val(payment.orderType)
		
		if (payment.method == "Money Remittance") {
			$('#referenceNumberForm').show()
			$('#remittanceBranchForm').show()

			$('#referenceNumber').val(payment.referenceNumber)
			$('#remittanceBranch').val(payment.remittanceBranch)
		}
	}

	if (typeof user == 'string') {
		user = JSON.parse(user)

		$('#firstname').val(user.cust_fname)
		$('#lastname').val(user.cust_lname)
		$('#email').val(user.cust_email)
		$('#mobilenumber').val(user.cust_cn)
	}

	function userReservation () {
		payment.total = finalPrice
		$.ajax({
	      type: 'POST',
	      url: '<?php echo $_ENV["base_url"]; ?>controllers/controller.php',
	      data: {
	      	cust_id: user.cust_id, 
	      	requestType: 'userReservation', 
	      	orderLine: orderLine, 
	      	payment: payment, 
	      	utility: utilityCart},
	      dataType: 'JSON',
	      success: function (data) {
	        if (data.status != 'OK') {
	          swal("Error!", data.message, "warning")
	          return;
	        }

	       	 window.location = "<?php echo $_ENV["base_url"]; ?>views/done.php";
	      },
	      error: function (data) {
	        swal("Oh no!", 'Server Error', "warning")
	      }
	    })
	}
</script>