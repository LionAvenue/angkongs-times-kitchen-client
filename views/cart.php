<?php require('../src/layouts/header.php');?>

	<div class="py-5" id="login">
		<div class="container py-xl-5 py-lg-3">
			<div class="row pt-lg-5 justify-content-md-center">
				<div class="col-sm-12 col-sm-offset-3 address-left wow agile fadeInLeft animated mt-lg-0 mt-5" data-wow-delay=".5s">
					<div class="address-grid p-sm-5 p-4">

					   <!-- Page Content -->
					  <div class="container">

					    <div class="row">
					    	<div class="col-lg-12">
					    	<h2>Menu Cart</h2>
					    		<table class="table table-striped table-bordered mt-5">
								  <thead>
								    <tr>
								      <th scope="col" width="20%">Menu</th>
								      <th scope="col" width="20%">Name</th>
								      <th scope="col" width="10%">Size</th>
								      <th scope="col" width="10%">Price</th>
								      <th scope="col" width="20%">Quantity</th>
								      <th scope="col" width="20%">Option</th>
								    </tr>
								  </thead>
								  <tbody id="menuRow">
								  </tbody>
								</table>
					    	</div>

					    	<div class="col-md-4 ml-auto">
					    		<div class="card">
								  <div class="card-body">
								    <h5 class="card-title">Summary</h5>
								    <p class="card-text">
							    	<form>
									  <div class="form-group">
									    <label for="exampleInputEmail1">No. of items:</label>
									  	 <label class="sr-only" for="menuSubTotal">Subtotal</label>
									      <div class="input-group mb-2">
									        <div class="input-group-prepend">
									          <div class="input-group-text">No.</div>
									        </div>
									        <input type="text" class="form-control" id="menuSubTotal" placeholder="Subtotal" readonly="true">
									      </div>
									  </div>
									  <div class="form-group">
									    <label for="exampleInputEmail1">Subtotal:</label>
									  	 <label class="sr-only" for="menuTotal">Total</label>
									      <div class="input-group mb-2">
									        <div class="input-group-prepend">
									          <div class="input-group-text"><span>&#8369;</span></div>
									        </div>
									        <input type="text" class="form-control" id="menuTotal" placeholder="Total" readonly="true">
									      </div>
									  </div>
									  </form>
								    </p>
								  </div>
								</div>

								<button class="btn btn-success btn-block mt-3" onclick="paymentMethod()">Proceed to Checkout</button>
					    	</div>

					    </div>
					    <!-- /.row -->

					  </div>

					</div>
				</div>
			</div>
		</div>
	</div>

	<style scope>
		.navbar {
		  /*background-color: #A1887F !important;*/
		  background-image: url("<?php echo $_ENV["base_url"]; ?>images/1.jpg") !important;
		}
	</style>


<?php require('../src/layouts/footer.php');?>

<script>
	let base_url_admin = "<?php echo $_ENV["base_url_admin"]; ?>";
	let menuCart = localStorage.getItem('menuCart') || [];
	let orderLine = localStorage.getItem('orderLine') || [];
	let payment = localStorage.getItem('payment') || {};
	let user = localStorage.getItem('user') || [];
	let subTotal = 0;
	let total = 0;
	let size_template = {
		'extra small': 'xs',
		'small': 's',
		'medium': 'm',
		'large': 'l'
	}

	if (!user.length) {
	 window.location = "<?php echo $_ENV["base_url"]; ?>views/login.php";
	}
	
	if (typeof payment == 'string') {
		payment = JSON.parse(payment);
	}

	if (typeof menuCart == 'string') {
		menuCart = JSON.parse(menuCart)
		template = ''

		for (let index = 0; index < menuCart.length; index++) {
			template = `<tr id='cartRow${menuCart[index].food_id}${size_template[menuCart[index].size]}'><td>
				<img class='img-thumbnail' src='${base_url_admin}/img/menu_img/${menuCart[index].food_image}' width='150px' alt=''>
			</td>
			<td>${menuCart[index].food_name}</td>
			<td>${menuCart[index].size}</td>
			<td>${menuCart[index].price_size}</td>
			<td>
				<div class='input-group mb-3'>
			  <div class='input-group-prepend'>
			    <button class='btn btn-outline-secondary' type='button' onclick='quantityModifier(${menuCart[index].food_id}, "-", "${menuCart[index].size}")'>-</button>
			  </div>
			  <input type='text' class='form-control' id='menuQuantity${menuCart[index].food_id}${size_template[menuCart[index].size]}' value='1' min='1' max='100' aria-label='' aria-describedby='basic-addon1' readonly>
			   <div class='input-group-append'>
			    <button class='btn btn-outline-secondary' type='button' onclick='quantityModifier(${menuCart[index].food_id}, "+", "${menuCart[index].size}")'>+</button>
			  </div>
			</div>
			</td>
			<td class='text-center'>
				<button class='btn btn-danger' onclick='removeToCart(${menuCart[index].food_id}, "${menuCart[index].size}")'>Remove</button>
			</td></tr>` + template
		}

		$('#menuRow').append(template)
	}

	if (typeof orderLine == 'string') {
		orderLine = JSON.parse(orderLine)	
		getOrderLine();
	} else {
		for (let index = 0; index < menuCart.length; index++) {
    		total = parseFloat(menuCart[index].price_size) + total
    		orderLine.push({
    			food_id: menuCart[index].food_id,
    			price: menuCart[index].price_size,
    			size: menuCart[index].size,
    			quantity: 1
    		})
    	}

		$('#menuSubTotal').val(orderLine.length)
		$('#menuTotal').val(parseFloat(total.toFixed(2)))
		payment.total = total;
		payment.subTotal = orderLine.length;
		localStorage.setItem("payment", JSON.stringify(payment))
		localStorage.setItem("orderLine", JSON.stringify(orderLine))
	}

	function getOrderLine () {
		for (let index = 0; index < menuCart.length; index++) {

			let exist = false

			for (let indexOder = 0; index < orderLine.length; indexOder ++) {
			    if (orderLine[indexOder].food_id === menuCart[index].food_id && orderLine[indexOder].size === menuCart[index].size) {
			     	exist = true
			     	break;
			    }
			}

			if (!exist) {
				orderLine.push({
				food_id: menuCart[index].food_id,
				price: menuCart[index].price_size,
				size: menuCart[index].size,
				quantity: 1
				})
			}
		}

		for (let index = 0; index < orderLine.length; index++) {
			subTotal = parseInt(orderLine[index].quantity) + subTotal

			total = (parseFloat(orderLine[index].price) * parseInt(orderLine[index].quantity)) + total
			$(`#menuQuantity${orderLine[index].food_id}${size_template[orderLine[index].size]}`).val(orderLine[index].quantity)
		}

		$('#menuTotal').val(total.toFixed(2))
		payment.total = total;
		payment.subTotal = subTotal
		localStorage.setItem("payment", JSON.stringify(payment))
		$('#menuSubTotal').val(subTotal)
		localStorage.setItem("orderLine", JSON.stringify(orderLine))			
	}

	function removeToCart (menu_id, size) {
		let exist = false
		let indexOrderItem = null
		let indexMenuItem = null

		for (let indexOder = 0; indexOder < menuCart.length; indexOder ++) {
		    if (parseInt(menuCart[indexOder].food_id) === menu_id && menuCart[indexOder].size === size) {
		     	exist = true
		     	indexMenuItem = indexOder
		     	break;
		    }
		}

		for (let index = 0; index < orderLine.length; index ++) {
		    if (parseInt(menuCart[index].food_id) === menu_id && menuCart[index].size === size) {
		    	indexOrderItem = index
		     	break;
		    }
		}

		if (exist) {

			if (indexMenuItem || indexMenuItem == 0) {
			    menuCart.splice(indexMenuItem, 1);
			}

			if (indexOrderItem || indexOrderItem == 0) {
			    orderLine.splice(indexOrderItem, 1);
			}

			subTotal = 0
			total = 0
			localStorage.setItem("orderLine", JSON.stringify(orderLine))
			localStorage.setItem("menuCart", JSON.stringify(menuCart))
		}

		if (orderLine.length == 0) {
			localStorage.removeItem('orderLine')
			localStorage.removeItem('menuCart')
		}

		getOrderLine()
		$(`#cartRow${menu_id}${size_template[size]}`).remove()
	}


	function quantityModifier (id, operator, size) {
		let quantity = parseInt($(`#menuQuantity${id}${size_template[size]}`).val());
		subTotal = 0
		total = 0

		console.log(quantity)
		let orderLineItem = orderLine.find(function (item) {
			return parseInt(item.food_id) === id; 
			})

		let orderIndex = null

		for (let index = 0; index < orderLine.length; index ++) {
		    if (parseInt(orderLine[index].food_id) === id && orderLine[index].size === size) {
		    	orderIndex = index
		     	break;
		    }
		}

		if (operator === '+') {
			quantity = quantity + 1
			orderLine[orderIndex].quantity = quantity
			localStorage.setItem("orderLine", JSON.stringify(orderLine))
			$('#menuTotal').val(total) 
			payment.total = total;
			// localStorage.setItem("payment", JSON.stringify(payment))
			$(`#menuQuantity${id}${size_template[size]}`).val(quantity)
		}

		if (operator === '-') {
			if (quantity > 1) {
				quantity = quantity - 1
				orderLine[orderIndex].quantity = quantity
				localStorage.setItem("orderLine", JSON.stringify(orderLine))
				$('#menuTotal').val(total)
				payment.total = total;
				// localStorage.setItem("payment", JSON.stringify(payment))
				$(`#menuQuantity${id}${size_template[size]}`).val(quantity)
			}
		}
	
		for (let index = 0; index < orderLine.length; index++) {
			total = (parseFloat(orderLine[index].price) * parseInt(orderLine[index].quantity)) + total
			subTotal = orderLine[index].quantity + subTotal
		}

		payment.subTotal = subTotal;
		payment.total = total;
		$('#menuSubTotal').val(subTotal)
		$('#menuTotal').val(total)
	  }

	function paymentMethod () {
		if (menuCart.length) {
			payment.orderType = 'menu';
			localStorage.setItem("payment", JSON.stringify(payment))
			window.location = "<?php echo $_ENV["base_url"]; ?>views/utilities.php";
		} else {
			swal('Wait!', 'Your Cart Is Empty!', 'info');
		}
	}
</script>